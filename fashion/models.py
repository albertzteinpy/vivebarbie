# -*- encoding: utf-8 -*-
from django.db import models
from django.contrib.auth.models import User
from mainapp.models import * 


TOY_CAT = [('0','cat1'),
		   ('1','cat2'),
		   ('2','cat3'),
		  ]




class Fashion_video(models.Model):

	background =  models.ImageField(u'Imagen movil',upload_to='fashion__videos')
	preview_ogv =  models.FileField(u'Pre ogv',upload_to='fashion__videos')
	preview_webm =  models.FileField(u'Pre webm',upload_to='fashion__videos')
	preview_mp4 =  models.FileField(u'Pre mp4',upload_to='fashion__videos')
	preview = models.ImageField(u'Preview carrusel',upload_to='fashion_preview_video')
	url = models.CharField(u'link Video',max_length=255)
	titulo = models.CharField(u'Título',max_length=255)
	puntos = models.IntegerField(u'Valor de Puntos',default=0)
	status = models.BooleanField(u'Visible',default=True)
	country = models.ManyToManyField(Country,related_name="fashion_country_video")

	def __unicode__(self):
		return u'%s'%self.titulo


class Fashiontoy(models.Model):
	nombre = models.CharField(u'Nombre',max_length=255)
	codigo = models.CharField(u'Codigo',max_length=255)
	descp = models.TextField(u'Descripción')
	puntos = models.IntegerField(u'Valor de Puntos')
	country = models.ManyToManyField(Country,related_name='fct')
	imagen =  models.ImageField(u'Imagen',upload_to='fashion_toys')
	titulo =  models.CharField(u'Título',max_length=200)
	texto =  models.CharField(u'Texto',max_length=200)
	imagen_car =  models.ImageField(u'Imagen Carousel',upload_to='fashion_toys')
	imagen_roll =  models.ImageField(u'Imagen Roll Over',upload_to='fashion_toys',blank=True,null=True)
	imagen_big =  models.ImageField(u'Imagen Big',upload_to='fashion_toys',blank=True,null=True)
	likes = models.IntegerField(u'likes',default=0,blank=True,null=True)
	cat =  models.CharField(u'categoría',max_length=200,choices=TOY_CAT)

	def __unicode__(self):
		return u'%s'%self.nombre


class Bulikesfashion(models.Model):
	userliker = models.ForeignKey(BarbieUser,related_name='like_fashion_usuario')
	toylike = models.ForeignKey(Fashiontoy,related_name='toy_like_fashion')
	date_like = models.DateTimeField(u'cuando',auto_now_add=True)

