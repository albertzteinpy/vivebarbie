from django.conf.urls.static import static
from django.conf import settings
from django.conf.urls import *

urlpatterns = patterns('fashion.views',
    #url(r'^fasihion/videos', 'juguetes', name='juguetes'),
    #url(r'^aventura-perritos/perritos_juguete', 'juguete', name='juguete'),
    #url(r'^aventura-perritos/descargas', 'descargas', name='descargas'),
    url(r'^fashion-studio/juego', 'juego', name='juego'),
    url(r'^fashion-studio/videos', 'videos', name='videos'),
    url(r'^fashion-studio/juguetes', 'juguetes', name='juguetes'),
    url(r'^fashion-studio/juguete/', 'juguete', name='juguete'),
    url(r'^fashion-studio/download', 'download', name='download'),
    url(r'^fashion-studio/likes', 'likes', name='likes'),
    url(r'fashion-studio/', 'home', name='home'),

)

