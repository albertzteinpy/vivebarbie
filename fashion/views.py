# -*- encoding: utf-8 -*-
from django.http import HttpResponse
from django.template.context import RequestContext
from django.shortcuts import get_object_or_404, render_to_response,render
from django.contrib.auth.decorators import login_required, permission_required
from django.shortcuts import redirect
import string as STrg
import datetime
from mainapp.models import *
from fashion.models import * 
#from redtranet_networking.forms import * 
import simplejson
from django.db.models import  get_app,get_models
from django.forms.models import model_to_dict
from django.contrib import auth
from django.core.mail import send_mail
from barbie.forms import * 
from django.contrib.auth.models import User
from mainapp.views import get_country,get_ip,country_by_url,FLAGS,get_title
from django.db.models import Q
import csv
from io import BytesIO
from StringIO import StringIO
import mimetypes
from PIL import Image
import os

from django.views.decorators.csrf import csrf_exempt

import base64 


@csrf_exempt
def download(request):
    


    filefi = '%s'%request.POST.get('f',None)
    fh = open("/var/www/devel_vivebarbie/static/descarga.png", "wb")
    fh.write(filefi.decode('base64'))
    fh.close()
    f = open("/var/www/devel_vivebarbie/static/descarga.png")
    response = HttpResponse(f,content_type="image/png")
    response['Content-Disposition'] = 'attachment; filename="%s%s"'%('caput2','png')
    return response



def home(request):
    args = {}
    context = RequestContext(request)
    mycountry = country_by_url(request)
    request.mycountryflag = FLAGS[mycountry]
    args['titulo']=get_title(request,'Fashion Studio')
    return render_to_response("fashion_home.html",args,context)



def juguetes(request):
    args = {}
    context = RequestContext(request)
    mycountry = country_by_url(request)
    request.mycountryflag = FLAGS[mycountry]
    args['titulo']=get_title(request,'Muñecas Fashion Studio')
    return render_to_response("fashion_juguetes.html",args,context)


def videos(request):
    args = {}
    context = RequestContext(request)
    mycountry = country_by_url(request)
    request.mycountryflag = FLAGS[mycountry]
    args['titulo']=get_title(request,' Videos Fashion Studio')
    return render_to_response("fashion_videos.html",args,context)


def juego(request):
    args = {}
    context = RequestContext(request)
    mycountry = country_by_url(request)
    request.mycountryflag = FLAGS[mycountry]
    args['titulo']=get_title(request,' Juego Fashion Studio')
    return render_to_response("fashion_juego.html",args,context)



def juguete(request):
    args = {}
    context = RequestContext(request)
    args['t']=request.GET.get('t',None)
    try: 
        toy = Fashiontoy.objects.get(codigo=args['t'])
    except: 
        toy = None
    args['toy']= toy

    try: 
        likes = Bulikesfashion.objects.get(toylike=toy,userliker=request.user.barbieuser_set.all()[0])
    except:
        likes = None
    
    args['liked']=likes

    return render_to_response("fashion_juguete.html",args,context)


def likes(request):
    toyliked = request.GET.get('t',None)
    if toyliked and request.user.is_staff:
        toyliker = Fashiontoy.objects.get(pk=toyliked)

        try: 
            barbieuser = request.user.barbieuser_set.all()[0]
        except:
            return HttpResponse('no es usuario valido')
    
        try:
            liked = Bulikesfashion.objects.get(userliker=barbieuser,toylike=toyliker)
            return HttpResponse('ya le diste like')
        except:
            liked = Bulikesfashion(userliker=barbieuser,toylike=toyliker)
            toyliker.likes += 1
            liked.save()
            toyliker.save()
            return HttpResponse('dandole like like')


    else:
        return HttpResponse('no like')