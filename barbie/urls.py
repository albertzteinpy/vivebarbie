from django.conf.urls import include, url
from django.contrib import admin
from django.conf.urls.static import static
from django.conf import settings
from django.views.generic import TemplateView

from django.contrib.sitemaps.views import sitemap


urlpatterns = [

    url(r'^', include('mainapp.urls')),
    url(r'^', include('campamentopop.urls')),
    url(r'^', include('perritos.urls')),
    url(r'^', include('superprincesas.urls')),
    url(r'^', include('fashion.urls')),
    url(r'^google3916edec6e930323.html', TemplateView.as_view(template_name='google/google.html')),
    #url(r'^sitemap.xml$', 'django.contrib.sitemaps.views.sitemap', {'sitemaps': sitemaps}),
    url(r'^admin/', include(admin.site.urls)),
]

urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)