# -*- encoding: utf-8 -*-
from django.http import HttpResponse
from django.template.context import RequestContext
from django.shortcuts import get_object_or_404, render_to_response,render
from django.contrib.auth.decorators import login_required, permission_required
from django.shortcuts import redirect
import string as STrg
import datetime
from mainapp.models import *
from campamentopop.models import * 
import simplejson
from django.db.models import  get_app,get_models
from django.forms.models import model_to_dict
from django.contrib import auth
from django.core.mail import send_mail
from barbie.forms import * 
from django.contrib.auth.models import User
from mainapp.views import get_country,get_ip,country_by_url,FLAGS,get_title
from io import BytesIO
from StringIO import StringIO
import mimetypes
from PIL import Image
import os
from slugify import slugify

# VIEWS'S SITE HERE -------------------------------------------------------------------------

def pop(request):

    args = {}
    context = RequestContext(request)
    real_country = country_by_url(request)
    args['c']=real_country
    args['inicio']='active'
    mycountry = country_by_url(request)
    request.mycountryflag = FLAGS[mycountry]
    args['titulo']=get_title(request,'Campamento Pop')
    return render_to_response("pop_home.html",args,context)


def registrate(request):
    return redirect('/registrate')

def promocion(request):

    args = {}
    context = RequestContext(request)
    real_country = country_by_url(request)
    args['c']=real_country
    args['inicio']='active'

    template = '%s_promociones.html'%real_country
    if real_country =='mx':
        images = 'img-promo-premios-imagen-e6d27683eba9589fd1ba9f7024e8e008852d0580e44e3cd370f0357221b87a7d.png'
    else:
        images = 'imgOtros-dd9ec728c35af2ba44eb6663c0c981cbde1edc56e2279914213671c9eb5c2894.png'

    args['images']=images
    args['promocion']='active'
    mycountry = country_by_url(request)
    request.mycountryflag = FLAGS[mycountry]
    args['titulo']=get_title(request,' Promoción Campamento Pop')
    return render_to_response(template,args,context)



def juguetes(request):
    args = {}
    context = RequestContext(request)
    
    country = country_by_url(request)
    real_country = get_country(request)

    if real_country == country:
        args['liketoy'] = True
    else:
        args['liketoy'] = None 
    if not country:
        country = real_country
    
    toys = Toy.objects.filter(country__subfix='%s'%country).order_by('order')
    args['toys']=toys


    try:
        likeds = Bulikes_campamento.objects.filter(userliker=request.user.barbieuser_set.all()[0])
        likeds_ids = [x.toylike.pk for x in likeds]
    except:
        likeds_ids = []

    args['likeds']=likeds_ids
    
    real_country = country_by_url(request)
    args['c']=real_country
    args['juguetes']='active'
    
    mycountry = country_by_url(request)
    request.mycountryflag = FLAGS[mycountry]
    args['titulo']=get_title(request,'Muñecas Campamento Pop')
    return render_to_response("pop_juguetes.html",args,context)

def juguete(request):
    args = {}
    context = RequestContext(request)
    
    id = request.GET.get('pk',None)
    try:
    	toys = Toy.objects.get(pk=id)
    except:
    	return HttpResponse('No toy regsitered')
    args['toys']=toys
    return render_to_response("pop_juguete.html",args,context)

def descargas(request):
    args = {}
    context = RequestContext(request)
    '''
    country = country_by_url(request)
    real_country = get_country(request)

    if real_country == country:
        args['liketoy'] = True
    else:
        args['liketoy'] = None 
    if not country:
        country = real_country
    downs = Descargable.objects.filter(country__subfix='%s'%country)
    args['downs'] = {}
    args['downs']['br']=downs.filter(cat='0')
    args['downs']['cr']=downs.filter(cat='1')
    args['downs']['w']=downs.filter(cat='2')
    '''
    real_country = country_by_url(request)
    args['c']=real_country
    args['descargas']='active'
    mycountry = country_by_url(request)
    request.mycountryflag = FLAGS[mycountry]
    args['titulo']=get_title(request,'Descargas Campamento Pop')
    return render_to_response("pop_descargas.html",args,context)



def asstxt(request):
    piezas = '''

        d:/static/pop_site/assets/assets:0:application/unknown
        t:/static/pop_site/assets/barbie.atlas:4301:application/unknown
        d:/static/pop_site/assets/shaders:0:application/unknown
        b:/static/pop_site/assets/shaders/screenspace.vertex:1001:application/unknown
        b:/static/pop_site/assets/shaders/crt-screen2.fragment:348:application/unknown
        b:/static/pop_site/assets/shaders/combine.fragment:2123:application/unknown
        i:/static/pop_site/assets/mainMenu.png:1334709:image/png
        t:/static/pop_site/assets/league_gothic_regular_62_highres.fnt:14270:application/unknown
        t:/static/pop_site/assets/mainMenu.atlas:3069:application/unknown
        t:/static/pop_site/assets/dollSkeleton.json:1563:application/unknown
        t:/static/pop_site/assets/museo_sans_500_24_highres.fnt:13756:application/unknown
        i:/static/pop_site/assets/barbie.png:639158:image/png
        i:/static/pop_site/assets/league_gothic_regular_62_highres.png:49136:image/png
        t:/static/pop_site/assets/mainMenu.json:970:application/unknown
        t:/static/pop_site/assets/game.atlas:1648:application/unknown
        i:/static/pop_site/assets/game.png:50036:image/png
        t:/static/pop_site/assets/game.json:845:application/unknown
        d:/static/pop_site/assets/music:0:application/unknown
        a:/static/pop_site/assets/music/buscate_en_la_cancion.mp3:632192:application/unknown
        a:/static/pop_site/assets/music/poder_soniar.mp3:780872:application/unknown
        a:/static/pop_site/assets/music/alcemos_nuestras_voces.mp3:639752:application/unknown
        a:/static/pop_site/assets/music/hacia_el_campamento.mp3:560552:application/unknown
        a:/static/pop_site/assets/music/voy_a_brillar.mp3:809312:application/unknown
        a:/static/pop_site/assets/music/show_final.mp3:569912:application/unknown
        a:/static/pop_site/assets/music/siendo_princesa.mp3:750272:application/unknown
        a:/static/pop_site/assets/click.mp3:6528:application/unknown
        i:/static/pop_site/assets/museo_sans_500_24_highres.png:23535:image/png
        i:/static/pop_site/assets/star.png:11569:image/png
        d:particles:0:application/unknown
        b:/static/pop_site/assets/particles/goodStar.p:2249:application/unknown
        b:/static/pop_site/assets/particles/confettiStars.p:4701:application/unknown
        b:/static/pop_site/assets/particles/flash.p:2147:application/unknown
        b:/static/pop_site/assets/particles/star.p:2693:application/unknown
        i:/static/pop_site/assets/game_bg.jpg:57896:image/jpeg
        t:/static/pop_site/assets/com/badlogic/gdx/graphics/g3d/particles/particles.fragment.glsl:770:application/unknown
        t:/static/pop_site/assets/com/badlogic/gdx/graphics/g3d/particles/particles.vertex.glsl:2886:application/unknown
        t:/static/pop_site/assets/com/badlogic/gdx/graphics/g3d/shaders/default.fragment.glsl:5163:application/unknown
        t:/static/pop_site/assets/com/badlogic/gdx/graphics/g3d/shaders/default.vertex.glsl:8948:application/unknown
        t:/static/pop_site/assets/com/badlogic/gdx/graphics/g3d/shaders/depth.fragment.glsl:870:application/unknown
        t:/static/pop_site/assets/com/badlogic/gdx/graphics/g3d/shaders/depth.vertex.glsl:2923:application/unknown
        t:/static/pop_site/assets/com/badlogic/gdx/utils/arial-15.fnt:21743:application/unknown
        i:/static/pop_site/assets/com/badlogic/gdx/utils/arial-15.png:21814:image/png

    '''

    return HttpResponse(piezas)


def download(request):
    filekey = request.GET.get('f',None)
    filename = '%s/static/pop/%s'%(os.path.dirname(os.path.dirname(os.path.abspath(__file__))),filekey)
    mm = mimetypes.guess_extension(filename)
    mtype = mimetypes.guess_type(filename)
    downloadame,extension = os.path.splitext(filename)
    datename = '%s'%datetime.datetime.now()
    nuname = '%s%s'%(slugify(datename),extension)
    f = open(filename)
    response = HttpResponse(f,content_type=mtype[0])

    dname_list = downloadame.split('/')
    dname = dname_list[len(dname_list)-1]


    response['Content-Disposition'] = 'attachment; filename="%s%s"'%(dname,extension)
    #response.write(f)
    return response
    #response = HttpResponse(content_type='image/png')
    #im = Image.open('/var/www/vivebarbie/static/pop/images/chi_txt.png')
    #response['Content-Disposition'] = 'attachment; filename="/var/www/vivebarbie/static/pop/images/chi_txt.png"'
    #header = PageHeader.objects.order_by('?')[0]
    #image = StringIO(file('/var/www/vivebarbie/static/pop/images/chi_txt.png', "rb").read())
    #mimetype = mimetypes.guess_type("/var/www/vivebarbie/static/pop/images/chi_txt.png")
    #assert False,mimetype
    #im.save(response , "png")
    #response.write(im)
    #return response
    #pdf = buffer.getvalue()
    #buffer.close()
    #response.write(pdf)
    #return response

def juego(request):
    args = {}
    context = RequestContext(request)
    real_country = country_by_url(request)
    args['c']=real_country
    args['juego']='active'
    mycountry = country_by_url(request)
    request.mycountryflag = FLAGS[mycountry]
    args['titulo']=get_title(request,'Juego Campamento Pop')
    return render_to_response("pop_juego.html",args,context)


def karaoke(request):
    args = {}
    context = RequestContext(request)
    args['titulo']=get_title(request,'Karaoke Campamento Pop')
    return render_to_response('pop_karaoke.html',args,context)

def backstage(request):
    args = {}
    context = RequestContext(request)
    real_country = country_by_url(request)
    args['c']=real_country
    #return redirect('/static/pop_site/karaoke.html')
    mycountry = country_by_url(request)
    request.mycountryflag = FLAGS[mycountry]
    args['titulo']=get_title(request,'Canciones Campamento Pop')
    return render_to_response("pop_backstage.html",args,context)




def videos(request):
    args = {}
    context = RequestContext(request)
    real_country = country_by_url(request)
    args['c']=real_country
    args['videos']='active'
    #return redirect('/static/pop_site/videos.html')
    mycountry = country_by_url(request)
    request.mycountryflag = FLAGS[mycountry]
    args['titulo']=get_title(request,'Videos Campamento Pop')
    return render_to_response("pop_videos.html",args,context)


def likes(request):
    toyliked = request.GET.get('t',None)

    if toyliked and request.user.is_staff:
        toyliker = Toy.objects.get(pk=toyliked)

        try: 
            barbieuser = request.user.barbieuser_set.all()[0]
        except:
            return HttpResponse('no es usuario valido')
    
        try:
            liked = Bulikes_campamento.objects.get(userliker=barbieuser,toylike=toyliker)
            return HttpResponse('ya le diste like')
        except:
            liked = Bulikes_campamento(userliker=barbieuser,toylike=toyliker)
            toyliker.likes += 1
            liked.save()
            toyliker.save()
            return HttpResponse('dandole like like')


    else:
        return HttpResponse('no like')

