from django.conf.urls.static import static
from django.conf import settings
from django.conf.urls import *

urlpatterns = patterns('campamentopop.views',
    url(r'^campamento-pop/juguetes', 'juguetes', name='juguetes'),
    url(r'^campamento-pop/juego', 'juego', name='juego'),
    url(r'^campamento-pop/videos', 'videos', name='videos'),
    url(r'^campamento-pop/descargas', 'descargas', name='descargas'),
    url(r'^campamento-pop/backstage', 'backstage', name='backstage'),
    url(r'^campamento-pop/karaoke', 'karaoke', name='karaoke'),
    url(r'^campamento-pop/download', 'download', name='download'),
    url(r'^campamento-pop/promocion', 'promocion', name='promocion'),
    url(r'^campamento-pop/registrate', 'registrate', name='registrate'),
    url(r'^campamento-pop/assets/assets.txt', 'asstxt', name='asstxt'),    
    url(r'^campamento-pop/likes', 'likes', name='likes'),
    url(r'^campamento-pop/', 'pop', name='pop'),

)

