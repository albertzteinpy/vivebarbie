/*! jQuery v1.11.0 | (c) 2005, 2014 jQuery Foundation, Inc. | jquery.org/license */
function onScrollInit() {
    $(".menu-top").removeClass("show")
}

function resizeFix() {
    var e = $(window).width(),
        t = $(window).height();
    768 >= e ? $("section").css({
        "min-height": t
    }) : $("section").css({
        "min-height": t - 293
    })
}

function initialize_player(e, t) {
    var n = t,
        i = e;
    jwplayer("jwplayer").setup({
        file: "http://www.youtube.com/watch?v=" + n,
        width: "100%",
        height: "100%",
        skin: {
            name: "five"
        },
        stretching: "exactfit",
        "controlbar.idlehide": "false",
        autostart: !0
    }).on("complete", function() {
        $.post(CALLBACK_VIDEO_URL.replace(":id", i), {
            id: i
        }, function(e) {
            e.video.created === !0 && $("#congratulations_modal").modal("show")
        })
    })
}

function playVideo(e, t) {
    var n;
    jQuery("#canciones-player").tubeplayer("destroy"), jQuery("#canciones-player").tubeplayer({
        width: 1,
        height: 1,
        allowFullScreen: "true",
        initialVideo: e,
        preferredQuality: "auto",
        showControls: !1,
        autoPlay: t,
        onPlay: function(e) {},
        onPause: function() {},
        onStop: function() {},
        onSeek: function(e) {},
        onMute: function() {},
        onUnMute: function() {},
        onPlayerUnstarted: function() {},
        onPlayerEnded: function() {
            clearInterval(n)
        },
        onPlayerPlaying: function() {
            clearInterval(n), n = setInterval(function() {
                updateProgressBar()
            }, 200)
        },
        onPlayerPaused: function() {
            clearInterval(n)
        },
        onPlayerBuffering: function() {},
        onPlayerCued: function() {},
        onQualityChange: function(e) {},
        onErrorNotFound: function() {},
        onErrorNotEmbeddable: function() {},
        onErrorInvalidParameter: function() {}
    })
}

function showData(e) {
    var t = "bytesLoaded : " + e.bytesLoaded;
    t += " / bytesTotal : " + e.bytesTotal + "\n", t += "currentTime : " + e.currentTime, t += " / duration : " + e.duration + "\n", t += "startBytes : " + e.startBytes + "\n", t += "state : " + e.state + "\n", t += "quality : " + e.availableQualityLevels.join(",") + "\n", t += "url : " + e.videoURL + "\n", t += "videoID : " + e.videoID
}

function updateProgressBar() {
    if (jQuery("#canciones-player").length > 0) {
        var e = jQuery("#canciones-player").tubeplayer("data"),
            t = e.currentTime,
            n = e.duration,
            i = t / n * 100;
        $("#progress-bar").css({
            width: i + "%"
        }), $("#progress-bar-dot").css({
            left: "calc(" + i + "% - 17px)"
        })
    }
}

function register_product(e) {
    var t = location.pathname.split("/")[1];
    $.post("/" + t + "/juguetes/" + e + "/register.json", {
        id: e
    }, function(e) {
        e.registration_created ? 1 == ASK_STORY ? ask_for_story() : $("#pop-thanks").removeClass("hide").addClass("show") : ($("#pop-estas").removeClass("show").addClass("hide"), alert("Ya has registrado este producto"))
    }, "json")
}

function like_product(e) {
    var t = location.pathname.split("/")[1];
    $.post("/" + t + "/juguetes/" + e + "/like.json", {
        id: e
    }, function(e) {
        e.likes && alert("Gracias por votar este producto")
    }, "json")
}

function ask_for_story() {
    $("#pop-estas").removeClass("show").addClass("hide"), $("#pop-registro").removeClass("hide").addClass("show"), $("#send-story").click(function() {
        $.post("/api/users/" + CURRENT_USER + "/story.json", {
            id: CURRENT_USER,
            story: $("#girl_story").val()
        }, function(e) {}, "json"), thanks(), ASK_STORY = !1
    })
}

function thanks() {
    $("#pop-registro").removeClass("show").addClass("hide"), $("#pop-registro-2").removeClass("hide").addClass("show")
}



/*Custom JS code Vivebarbie */


