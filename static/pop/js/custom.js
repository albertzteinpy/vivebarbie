!function(e) {
    "function" == typeof define && define.amd ? define(["jquery"], e) : "object" == typeof exports ? module.exports = e : e(jQuery)
}(function(e) {
    function t(t) {
        var a = t || window.event,
            s = l.call(arguments, 1),
            c = 0,
            d = 0,
            p = 0,
            h = 0,
            f = 0,
            g = 0;
        if (t = e.event.fix(a), t.type = "mousewheel", "detail" in a && (p = -1 * a.detail), "wheelDelta" in a && (p = a.wheelDelta), "wheelDeltaY" in a && (p = a.wheelDeltaY), "wheelDeltaX" in a && (d = -1 * a.wheelDeltaX), "axis" in a && a.axis === a.HORIZONTAL_AXIS && (d = -1 * p, p = 0), c = 0 === p ? d : p, "deltaY" in a && (p = -1 * a.deltaY, c = p), "deltaX" in a && (d = a.deltaX, 0 === p && (c = -1 * d)), 0 !== p || 0 !== d) {
            if (1 === a.deltaMode) {
                var m = e.data(this, "mousewheel-line-height");
                c *= m, p *= m, d *= m
            } else if (2 === a.deltaMode) {
                var w = e.data(this, "mousewheel-page-height");
                c *= w, p *= w, d *= w
            }
            if (h = Math.max(Math.abs(p), Math.abs(d)), (!r || r > h) && (r = h, i(a, h) && (r /= 40)), i(a, h) && (c /= 40, d /= 40, p /= 40), c = Math[c >= 1 ? "floor" : "ceil"](c / r), d = Math[d >= 1 ? "floor" : "ceil"](d / r), p = Math[p >= 1 ? "floor" : "ceil"](p / r), u.settings.normalizeOffset && this.getBoundingClientRect) {
                var v = this.getBoundingClientRect();
                f = t.clientX - v.left, g = t.clientY - v.top
            }
            return t.deltaX = d, t.deltaY = p, t.deltaFactor = r, t.offsetX = f, t.offsetY = g, t.deltaMode = 0, s.unshift(t, c, d, p), o && clearTimeout(o), o = setTimeout(n, 200), (e.event.dispatch || e.event.handle).apply(this, s)
        }
    }

    function n() {
        r = null
    }

    function i(e, t) {
        return u.settings.adjustOldDeltas && "mousewheel" === e.type && t % 120 === 0
    }
    var o, r, a = ["wheel", "mousewheel", "DOMMouseScroll", "MozMousePixelScroll"],
        s = "onwheel" in document || document.documentMode >= 9 ? ["wheel"] : ["mousewheel", "DomMouseScroll", "MozMousePixelScroll"],
        l = Array.prototype.slice;
    if (e.event.fixHooks)
        for (var c = a.length; c;) e.event.fixHooks[a[--c]] = e.event.mouseHooks;
    var u = e.event.special.mousewheel = {
        version: "3.1.12",
        setup: function() {
            if (this.addEventListener)
                for (var n = s.length; n;) this.addEventListener(s[--n], t, !1);
            else this.onmousewheel = t;
            e.data(this, "mousewheel-line-height", u.getLineHeight(this)), e.data(this, "mousewheel-page-height", u.getPageHeight(this))
        },
        teardown: function() {
            if (this.removeEventListener)
                for (var n = s.length; n;) this.removeEventListener(s[--n], t, !1);
            else this.onmousewheel = null;
            e.removeData(this, "mousewheel-line-height"), e.removeData(this, "mousewheel-page-height")
        },
        getLineHeight: function(t) {
            var n = e(t),
                i = n["offsetParent" in e.fn ? "offsetParent" : "parent"]();
            return i.length || (i = e("body")), parseInt(i.css("fontSize"), 10) || parseInt(n.css("fontSize"), 10) || 16
        },
        getPageHeight: function(t) {
            return e(t).height()
        },
        settings: {
            adjustOldDeltas: !0,
            normalizeOffset: !0
        }
    };
    e.fn.extend({
        mousewheel: function(e) {
            return e ? this.bind("mousewheel", e) : this.trigger("mousewheel")
        },
        unmousewheel: function(e) {
            return this.unbind("mousewheel", e)
        }
    })
}), ! function(e) {
    "undefined" != typeof module && module.exports ? module.exports = e : e(jQuery, window, document)
}(function(e) {
    ! function(t) {
        var n = "function" == typeof define && define.amd,
            i = "undefined" != typeof module && module.exports,
            o = "https:" == document.location.protocol ? "https:" : "http:",
            r = "cdnjs.cloudflare.com/ajax/libs/jquery-mousewheel/3.1.12/jquery.mousewheel.min.js";
        n || (i ? require("jquery-mousewheel")(e) : e.event.special.mousewheel || e("head").append(decodeURI("%3Cscript src=" + o + "//" + r + "%3E%3C/script%3E"))), t()
    }(function() {
        var t, n = "mCustomScrollbar",
            i = "mCS",
            o = ".mCustomScrollbar",
            r = {
                setTop: 0,
                setLeft: 0,
                axis: "y",
                scrollbarPosition: "inside",
                scrollInertia: 950,
                autoDraggerLength: !0,
                alwaysShowScrollbar: 0,
                snapOffset: 0,
                mouseWheel: {
                    enable: !0,
                    scrollAmount: "auto",
                    axis: "y",
                    deltaFactor: "auto",
                    disableOver: ["select", "option", "keygen", "datalist", "textarea"]
                },
                scrollButtons: {
                    scrollType: "stepless",
                    scrollAmount: "auto"
                },
                keyboard: {
                    enable: !0,
                    scrollType: "stepless",
                    scrollAmount: "auto"
                },
                contentTouchScroll: 25,
                advanced: {
                    autoScrollOnFocus: "input,textarea,select,button,datalist,keygen,a[tabindex],area,object,[contenteditable='true']",
                    updateOnContentResize: !0,
                    updateOnImageLoad: !0,
                    autoUpdateTimeout: 60
                },
                theme: "light",
                callbacks: {
                    onTotalScrollOffset: 0,
                    onTotalScrollBackOffset: 0,
                    alwaysTriggerOffsets: !0
                }
            }, a = 0,
            s = {}, l = window.attachEvent && !window.addEventListener ? 1 : 0,
            c = !1,
            u = ["mCSB_dragger_onDrag", "mCSB_scrollTools_onDrag", "mCS_img_loaded", "mCS_disabled", "mCS_destroyed", "mCS_no_scrollbar", "mCS-autoHide", "mCS-dir-rtl", "mCS_no_scrollbar_y", "mCS_no_scrollbar_x", "mCS_y_hidden", "mCS_x_hidden", "mCSB_draggerContainer", "mCSB_buttonUp", "mCSB_buttonDown", "mCSB_buttonLeft", "mCSB_buttonRight"],
            d = {
                init: function(t) {
                    var t = e.extend(!0, {}, r, t),
                        n = p.call(this);
                    if (t.live) {
                        var l = t.liveSelector || this.selector || o,
                            c = e(l);
                        if ("off" === t.live) return void f(l);
                        s[l] = setTimeout(function() {
                            c.mCustomScrollbar(t), "once" === t.live && c.length && f(l)
                        }, 500)
                    } else f(l);
                    return t.setWidth = t.set_width ? t.set_width : t.setWidth, t.setHeight = t.set_height ? t.set_height : t.setHeight, t.axis = t.horizontalScroll ? "x" : g(t.axis), t.scrollInertia = t.scrollInertia > 0 && t.scrollInertia < 17 ? 17 : t.scrollInertia, "object" != typeof t.mouseWheel && 1 == t.mouseWheel && (t.mouseWheel = {
                        enable: !0,
                        scrollAmount: "auto",
                        axis: "y",
                        preventDefault: !1,
                        deltaFactor: "auto",
                        normalizeDelta: !1,
                        invert: !1
                    }), t.mouseWheel.scrollAmount = t.mouseWheelPixels ? t.mouseWheelPixels : t.mouseWheel.scrollAmount, t.mouseWheel.normalizeDelta = t.advanced.normalizeMouseWheelDelta ? t.advanced.normalizeMouseWheelDelta : t.mouseWheel.normalizeDelta, t.scrollButtons.scrollType = m(t.scrollButtons.scrollType), h(t), e(n).each(function() {
                        var n = e(this);
                        if (!n.data(i)) {
                            n.data(i, {
                                idx: ++a,
                                opt: t,
                                scrollRatio: {
                                    y: null,
                                    x: null
                                },
                                overflowed: null,
                                contentReset: {
                                    y: null,
                                    x: null
                                },
                                bindEvents: !1,
                                tweenRunning: !1,
                                sequential: {},
                                langDir: n.css("direction"),
                                cbOffsets: null,
                                trigger: null
                            });
                            var o = n.data(i),
                                r = o.opt,
                                s = n.data("mcs-axis"),
                                l = n.data("mcs-scrollbar-position"),
                                c = n.data("mcs-theme");
                            s && (r.axis = s), l && (r.scrollbarPosition = l), c && (r.theme = c, h(r)), w.call(this), e("#mCSB_" + o.idx + "_container img:not(." + u[2] + ")").addClass(u[2]), d.update.call(null, n)
                        }
                    })
                },
                update: function(t, n) {
                    var o = t || p.call(this);
                    return e(o).each(function() {
                        var t = e(this);
                        if (t.data(i)) {
                            var o = t.data(i),
                                r = o.opt,
                                a = e("#mCSB_" + o.idx + "_container"),
                                s = [e("#mCSB_" + o.idx + "_dragger_vertical"), e("#mCSB_" + o.idx + "_dragger_horizontal")];
                            if (!a.length) return;
                            o.tweenRunning && V(t), t.hasClass(u[3]) && t.removeClass(u[3]), t.hasClass(u[4]) && t.removeClass(u[4]), b.call(this), A.call(this), "y" === r.axis || r.advanced.autoExpandHorizontalScroll || a.css("width", v(a.children())), o.overflowed = x.call(this), _.call(this), r.autoDraggerLength && j.call(this), k.call(this), D.call(this);
                            var l = [Math.abs(a[0].offsetTop), Math.abs(a[0].offsetLeft)];
                            "x" !== r.axis && (o.overflowed[0] ? s[0].height() > s[0].parent().height() ? C.call(this) : (G(t, l[0].toString(), {
                                dir: "y",
                                dur: 0,
                                overwrite: "none"
                            }), o.contentReset.y = null) : (C.call(this), "y" === r.axis ? T.call(this) : "yx" === r.axis && o.overflowed[1] && G(t, l[1].toString(), {
                                dir: "x",
                                dur: 0,
                                overwrite: "none"
                            }))), "y" !== r.axis && (o.overflowed[1] ? s[1].width() > s[1].parent().width() ? C.call(this) : (G(t, l[1].toString(), {
                                dir: "x",
                                dur: 0,
                                overwrite: "none"
                            }), o.contentReset.x = null) : (C.call(this), "x" === r.axis ? T.call(this) : "yx" === r.axis && o.overflowed[0] && G(t, l[0].toString(), {
                                dir: "y",
                                dur: 0,
                                overwrite: "none"
                            }))), n && o && (2 === n && r.callbacks.onImageLoad && "function" == typeof r.callbacks.onImageLoad ? r.callbacks.onImageLoad.call(this) : 3 === n && r.callbacks.onSelectorChange && "function" == typeof r.callbacks.onSelectorChange ? r.callbacks.onSelectorChange.call(this) : r.callbacks.onUpdate && "function" == typeof r.callbacks.onUpdate && r.callbacks.onUpdate.call(this)), z.call(this)
                        }
                    })
                },
                scrollTo: function(t, n) {
                    if ("undefined" != typeof t && null != t) {
                        var o = p.call(this);
                        return e(o).each(function() {
                            var o = e(this);
                            if (o.data(i)) {
                                var r = o.data(i),
                                    a = r.opt,
                                    s = {
                                        trigger: "external",
                                        scrollInertia: a.scrollInertia,
                                        scrollEasing: "mcsEaseInOut",
                                        moveDragger: !1,
                                        timeout: 60,
                                        callbacks: !0,
                                        onStart: !0,
                                        onUpdate: !0,
                                        onComplete: !0
                                    }, l = e.extend(!0, {}, s, n),
                                    c = H.call(this, t),
                                    u = l.scrollInertia > 0 && l.scrollInertia < 17 ? 17 : l.scrollInertia;
                                c[0] = W.call(this, c[0], "y"), c[1] = W.call(this, c[1], "x"), l.moveDragger && (c[0] *= r.scrollRatio.y, c[1] *= r.scrollRatio.x), l.dur = u, setTimeout(function() {
                                    null !== c[0] && "undefined" != typeof c[0] && "x" !== a.axis && r.overflowed[0] && (l.dir = "y", l.overwrite = "all", G(o, c[0].toString(), l)), null !== c[1] && "undefined" != typeof c[1] && "y" !== a.axis && r.overflowed[1] && (l.dir = "x", l.overwrite = "none", G(o, c[1].toString(), l))
                                }, l.timeout)
                            }
                        })
                    }
                },
                stop: function() {
                    var t = p.call(this);
                    return e(t).each(function() {
                        var t = e(this);
                        t.data(i) && V(t)
                    })
                },
                disable: function(t) {
                    var n = p.call(this);
                    return e(n).each(function() {
                        var n = e(this);
                        n.data(i) && (n.data(i), z.call(this, "remove"), T.call(this), t && C.call(this), _.call(this, !0), n.addClass(u[3]))
                    })
                },
                destroy: function() {
                    var t = p.call(this);
                    return e(t).each(function() {
                        var o = e(this);
                        if (o.data(i)) {
                            var r = o.data(i),
                                a = r.opt,
                                s = e("#mCSB_" + r.idx),
                                l = e("#mCSB_" + r.idx + "_container"),
                                c = e(".mCSB_" + r.idx + "_scrollbar");
                            a.live && f(a.liveSelector || e(t).selector), z.call(this, "remove"), T.call(this), C.call(this), o.removeData(i), K(this, "mcs"), c.remove(), l.find("img." + u[2]).removeClass(u[2]), s.replaceWith(l.contents()), o.removeClass(n + " _" + i + "_" + r.idx + " " + u[6] + " " + u[7] + " " + u[5] + " " + u[3]).addClass(u[4])
                        }
                    })
                }
            }, p = function() {
                return "object" != typeof e(this) || e(this).length < 1 ? o : this
            }, h = function(t) {
                var n = ["rounded", "rounded-dark", "rounded-dots", "rounded-dots-dark"],
                    i = ["rounded-dots", "rounded-dots-dark", "3d", "3d-dark", "3d-thick", "3d-thick-dark", "inset", "inset-dark", "inset-2", "inset-2-dark", "inset-3", "inset-3-dark"],
                    o = ["minimal", "minimal-dark"],
                    r = ["minimal", "minimal-dark"],
                    a = ["minimal", "minimal-dark"];
                t.autoDraggerLength = e.inArray(t.theme, n) > -1 ? !1 : t.autoDraggerLength, t.autoExpandScrollbar = e.inArray(t.theme, i) > -1 ? !1 : t.autoExpandScrollbar, t.scrollButtons.enable = e.inArray(t.theme, o) > -1 ? !1 : t.scrollButtons.enable, t.autoHideScrollbar = e.inArray(t.theme, r) > -1 ? !0 : t.autoHideScrollbar, t.scrollbarPosition = e.inArray(t.theme, a) > -1 ? "outside" : t.scrollbarPosition
            }, f = function(e) {
                s[e] && (clearTimeout(s[e]), K(s, e))
            }, g = function(e) {
                return "yx" === e || "xy" === e || "auto" === e ? "yx" : "x" === e || "horizontal" === e ? "x" : "y"
            }, m = function(e) {
                return "stepped" === e || "pixels" === e || "step" === e || "click" === e ? "stepped" : "stepless"
            }, w = function() {
                var t = e(this),
                    o = t.data(i),
                    r = o.opt,
                    a = r.autoExpandScrollbar ? " " + u[1] + "_expand" : "",
                    s = ["<div id='mCSB_" + o.idx + "_scrollbar_vertical' class='mCSB_scrollTools mCSB_" + o.idx + "_scrollbar mCS-" + r.theme + " mCSB_scrollTools_vertical" + a + "'><div class='" + u[12] + "'><div id='mCSB_" + o.idx + "_dragger_vertical' class='mCSB_dragger' style='position:absolute;' oncontextmenu='return false;'><div class='mCSB_dragger_bar' /></div><div class='mCSB_draggerRail' /></div></div>", "<div id='mCSB_" + o.idx + "_scrollbar_horizontal' class='mCSB_scrollTools mCSB_" + o.idx + "_scrollbar mCS-" + r.theme + " mCSB_scrollTools_horizontal" + a + "'><div class='" + u[12] + "'><div id='mCSB_" + o.idx + "_dragger_horizontal' class='mCSB_dragger' style='position:absolute;' oncontextmenu='return false;'><div class='mCSB_dragger_bar' /></div><div class='mCSB_draggerRail' /></div></div>"],
                    l = "yx" === r.axis ? "mCSB_vertical_horizontal" : "x" === r.axis ? "mCSB_horizontal" : "mCSB_vertical",
                    c = "yx" === r.axis ? s[0] + s[1] : "x" === r.axis ? s[1] : s[0],
                    d = "yx" === r.axis ? "<div id='mCSB_" + o.idx + "_container_wrapper' class='mCSB_container_wrapper' />" : "",
                    p = r.autoHideScrollbar ? " " + u[6] : "",
                    h = "x" !== r.axis && "rtl" === o.langDir ? " " + u[7] : "";
                r.setWidth && t.css("width", r.setWidth), r.setHeight && t.css("height", r.setHeight), r.setLeft = "y" !== r.axis && "rtl" === o.langDir ? "989999px" : r.setLeft, t.addClass(n + " _" + i + "_" + o.idx + p + h).wrapInner("<div id='mCSB_" + o.idx + "' class='mCustomScrollBox mCS-" + r.theme + " " + l + "'><div id='mCSB_" + o.idx + "_container' class='mCSB_container' style='position:relative; top:" + r.setTop + "; left:" + r.setLeft + ";' dir=" + o.langDir + " /></div>");
                var f = e("#mCSB_" + o.idx),
                    g = e("#mCSB_" + o.idx + "_container");
                "y" === r.axis || r.advanced.autoExpandHorizontalScroll || g.css("width", v(g.children())), "outside" === r.scrollbarPosition ? ("static" === t.css("position") && t.css("position", "relative"), t.css("overflow", "visible"), f.addClass("mCSB_outside").after(c)) : (f.addClass("mCSB_inside").append(c), g.wrap(d)), y.call(this);
                var m = [e("#mCSB_" + o.idx + "_dragger_vertical"), e("#mCSB_" + o.idx + "_dragger_horizontal")];
                m[0].css("min-height", m[0].height()), m[1].css("min-width", m[1].width())
            }, v = function(t) {
                return Math.max.apply(Math, t.map(function() {
                    return e(this).outerWidth(!0)
                }).get())
            }, A = function() {
                var t = e(this),
                    n = t.data(i),
                    o = n.opt,
                    r = e("#mCSB_" + n.idx + "_container");
                o.advanced.autoExpandHorizontalScroll && "y" !== o.axis && r.css({
                    position: "absolute",
                    width: "auto"
                }).wrap("<div class='mCSB_h_wrapper' style='position:relative; left:0; width:999999px;' />").css({
                    width: Math.ceil(r[0].getBoundingClientRect().right + .4) - Math.floor(r[0].getBoundingClientRect().left),
                    position: "relative"
                }).unwrap()
            }, y = function() {
                var t = e(this),
                    n = t.data(i),
                    o = n.opt,
                    r = e(".mCSB_" + n.idx + "_scrollbar:first"),
                    a = te(o.scrollButtons.tabindex) ? "tabindex='" + o.scrollButtons.tabindex + "'" : "",
                    s = ["<a href='#' class='" + u[13] + "' oncontextmenu='return false;' " + a + " />", "<a href='#' class='" + u[14] + "' oncontextmenu='return false;' " + a + " />", "<a href='#' class='" + u[15] + "' oncontextmenu='return false;' " + a + " />", "<a href='#' class='" + u[16] + "' oncontextmenu='return false;' " + a + " />"],
                    l = ["x" === o.axis ? s[2] : s[0], "x" === o.axis ? s[3] : s[1], s[2], s[3]];
                o.scrollButtons.enable && r.prepend(l[0]).append(l[1]).next(".mCSB_scrollTools").prepend(l[2]).append(l[3])
            }, b = function() {
                var t = e(this),
                    n = t.data(i),
                    o = e("#mCSB_" + n.idx),
                    r = t.css("max-height") || "none",
                    a = -1 !== r.indexOf("%"),
                    s = t.css("box-sizing");
                if ("none" !== r) {
                    var l = a ? t.parent().height() * parseInt(r) / 100 : parseInt(r);
                    "border-box" === s && (l -= t.innerHeight() - t.height() + (t.outerHeight() - t.innerHeight())), o.css("max-height", Math.round(l))
                }
            }, j = function() {
                var t = e(this),
                    n = t.data(i),
                    o = e("#mCSB_" + n.idx),
                    r = e("#mCSB_" + n.idx + "_container"),
                    a = [e("#mCSB_" + n.idx + "_dragger_vertical"), e("#mCSB_" + n.idx + "_dragger_horizontal")],
                    s = [o.height() / r.outerHeight(!1), o.width() / r.outerWidth(!1)],
                    c = [parseInt(a[0].css("min-height")), Math.round(s[0] * a[0].parent().height()), parseInt(a[1].css("min-width")), Math.round(s[1] * a[1].parent().width())],
                    u = l && c[1] < c[0] ? c[0] : c[1],
                    d = l && c[3] < c[2] ? c[2] : c[3];
                a[0].css({
                    height: u,
                    "max-height": a[0].parent().height() - 10
                }).find(".mCSB_dragger_bar").css({
                    "line-height": c[0] + "px"
                }), a[1].css({
                    width: d,
                    "max-width": a[1].parent().width() - 10
                })
            }, k = function() {
                var t = e(this),
                    n = t.data(i),
                    o = e("#mCSB_" + n.idx),
                    r = e("#mCSB_" + n.idx + "_container"),
                    a = [e("#mCSB_" + n.idx + "_dragger_vertical"), e("#mCSB_" + n.idx + "_dragger_horizontal")],
                    s = [r.outerHeight(!1) - o.height(), r.outerWidth(!1) - o.width()],
                    l = [s[0] / (a[0].parent().height() - a[0].height()), s[1] / (a[1].parent().width() - a[1].width())];
                n.scrollRatio = {
                    y: l[0],
                    x: l[1]
                }
            }, E = function(e, t, n) {
                var i = n ? u[0] + "_expanded" : "",
                    o = e.closest(".mCSB_scrollTools");
                "active" === t ? (e.toggleClass(u[0] + " " + i), o.toggleClass(u[1]), e[0]._draggable = e[0]._draggable ? 0 : 1) : e[0]._draggable || ("hide" === t ? (e.removeClass(u[0]), o.removeClass(u[1])) : (e.addClass(u[0]), o.addClass(u[1])))
            }, x = function() {
                var t = e(this),
                    n = t.data(i),
                    o = e("#mCSB_" + n.idx),
                    r = e("#mCSB_" + n.idx + "_container"),
                    a = null == n.overflowed ? r.height() : r.outerHeight(!1),
                    s = null == n.overflowed ? r.width() : r.outerWidth(!1);
                return [a > o.height(), s > o.width()]
            }, C = function() {
                var t = e(this),
                    n = t.data(i),
                    o = n.opt,
                    r = e("#mCSB_" + n.idx),
                    a = e("#mCSB_" + n.idx + "_container"),
                    s = [e("#mCSB_" + n.idx + "_dragger_vertical"), e("#mCSB_" + n.idx + "_dragger_horizontal")];
                if (V(t), ("x" !== o.axis && !n.overflowed[0] || "y" === o.axis && n.overflowed[0]) && (s[0].add(a).css("top", 0), G(t, "_resetY")), "y" !== o.axis && !n.overflowed[1] || "x" === o.axis && n.overflowed[1]) {
                    var l = dx = 0;
                    "rtl" === n.langDir && (l = r.width() - a.outerWidth(!1), dx = Math.abs(l / n.scrollRatio.x)), a.css("left", l), s[1].css("left", dx), G(t, "_resetX")
                }
            }, D = function() {
                function t() {
                    a = setTimeout(function() {
                        e.event.special.mousewheel ? (clearTimeout(a), R.call(n[0])) : t()
                    }, 100)
                }
                var n = e(this),
                    o = n.data(i),
                    r = o.opt;
                if (!o.bindEvents) {
                    if (L.call(this), r.contentTouchScroll && M.call(this), I.call(this), r.mouseWheel.enable) {
                        var a;
                        t()
                    }
                    N.call(this), O.call(this), r.advanced.autoScrollOnFocus && F.call(this), r.scrollButtons.enable && U.call(this), r.keyboard.enable && Y.call(this), o.bindEvents = !0
                }
            }, T = function() {
                var t = e(this),
                    n = t.data(i),
                    o = n.opt,
                    r = i + "_" + n.idx,
                    a = ".mCSB_" + n.idx + "_scrollbar",
                    s = e("#mCSB_" + n.idx + ",#mCSB_" + n.idx + "_container,#mCSB_" + n.idx + "_container_wrapper," + a + " ." + u[12] + ",#mCSB_" + n.idx + "_dragger_vertical,#mCSB_" + n.idx + "_dragger_horizontal," + a + ">a"),
                    l = e("#mCSB_" + n.idx + "_container");
                o.advanced.releaseDraggableSelectors && s.add(e(o.advanced.releaseDraggableSelectors)), n.bindEvents && (e(document).unbind("." + r), s.each(function() {
                    e(this).unbind("." + r)
                }), clearTimeout(t[0]._focusTimeout), K(t[0], "_focusTimeout"), clearTimeout(n.sequential.step), K(n.sequential, "step"), clearTimeout(l[0].onCompleteTimeout), K(l[0], "onCompleteTimeout"), n.bindEvents = !1)
            }, _ = function(t) {
                var n = e(this),
                    o = n.data(i),
                    r = o.opt,
                    a = e("#mCSB_" + o.idx + "_container_wrapper"),
                    s = a.length ? a : e("#mCSB_" + o.idx + "_container"),
                    l = [e("#mCSB_" + o.idx + "_scrollbar_vertical"), e("#mCSB_" + o.idx + "_scrollbar_horizontal")],
                    c = [l[0].find(".mCSB_dragger"), l[1].find(".mCSB_dragger")];
                "x" !== r.axis && (o.overflowed[0] && !t ? (l[0].add(c[0]).add(l[0].children("a")).css("display", "block"), s.removeClass(u[8] + " " + u[10])) : (r.alwaysShowScrollbar ? (2 !== r.alwaysShowScrollbar && c[0].css("display", "none"), s.removeClass(u[10])) : (l[0].css("display", "none"), s.addClass(u[10])), s.addClass(u[8]))), "y" !== r.axis && (o.overflowed[1] && !t ? (l[1].add(c[1]).add(l[1].children("a")).css("display", "block"), s.removeClass(u[9] + " " + u[11])) : (r.alwaysShowScrollbar ? (2 !== r.alwaysShowScrollbar && c[1].css("display", "none"), s.removeClass(u[11])) : (l[1].css("display", "none"), s.addClass(u[11])), s.addClass(u[9]))), o.overflowed[0] || o.overflowed[1] ? n.removeClass(u[5]) : n.addClass(u[5])
            }, S = function(e) {
                var t = e.type;
                switch (t) {
                    case "pointerdown":
                    case "MSPointerDown":
                    case "pointermove":
                    case "MSPointerMove":
                    case "pointerup":
                    case "MSPointerUp":
                        return e.target.ownerDocument !== document ? [e.originalEvent.screenY, e.originalEvent.screenX, !1] : [e.originalEvent.pageY, e.originalEvent.pageX, !1];
                    case "touchstart":
                    case "touchmove":
                    case "touchend":
                        var n = e.originalEvent.touches[0] || e.originalEvent.changedTouches[0],
                            i = e.originalEvent.touches.length || e.originalEvent.changedTouches.length;
                        return e.target.ownerDocument !== document ? [n.screenY, n.screenX, i > 1] : [n.pageY, n.pageX, i > 1];
                    default:
                        return [e.pageY, e.pageX, !1]
                }
            }, L = function() {
                function t(e) {
                    var t = f.find("iframe");
                    if (t.length) {
                        var n = e ? "auto" : "none";
                        t.css("pointer-events", n)
                    }
                }

                function n(e, t, n, i) {
                    if (f[0].idleTimer = d.scrollInertia < 233 ? 250 : 0, o.attr("id") === h[1]) var r = "x",
                    a = (o[0].offsetLeft - t + i) * u.scrollRatio.x;
                    else var r = "y",
                    a = (o[0].offsetTop - e + n) * u.scrollRatio.y;
                    G(s, a.toString(), {
                        dir: r,
                        drag: !0
                    })
                }
                var o, r, a, s = e(this),
                    u = s.data(i),
                    d = u.opt,
                    p = i + "_" + u.idx,
                    h = ["mCSB_" + u.idx + "_dragger_vertical", "mCSB_" + u.idx + "_dragger_horizontal"],
                    f = e("#mCSB_" + u.idx + "_container"),
                    g = e("#" + h[0] + ",#" + h[1]),
                    m = d.advanced.releaseDraggableSelectors ? g.add(e(d.advanced.releaseDraggableSelectors)) : g;
                g.bind("mousedown." + p + " touchstart." + p + " pointerdown." + p + " MSPointerDown." + p, function(n) {
                    if (n.stopImmediatePropagation(), n.preventDefault(), Z(n)) {
                        c = !0, l && (document.onselectstart = function() {
                            return !1
                        }), t(!1), V(s), o = e(this);
                        var i = o.offset(),
                            u = S(n)[0] - i.top,
                            p = S(n)[1] - i.left,
                            h = o.height() + i.top,
                            f = o.width() + i.left;
                        h > u && u > 0 && f > p && p > 0 && (r = u, a = p), E(o, "active", d.autoExpandScrollbar)
                    }
                }).bind("touchmove." + p, function(e) {
                    e.stopImmediatePropagation(), e.preventDefault();
                    var t = o.offset(),
                        i = S(e)[0] - t.top,
                        s = S(e)[1] - t.left;
                    n(r, a, i, s)
                }), e(document).bind("mousemove." + p + " pointermove." + p + " MSPointerMove." + p, function(e) {
                    if (o) {
                        var t = o.offset(),
                            i = S(e)[0] - t.top,
                            s = S(e)[1] - t.left;
                        if (r === i) return;
                        n(r, a, i, s)
                    }
                }).add(m).bind("mouseup." + p + " touchend." + p + " pointerup." + p + " MSPointerUp." + p, function(e) {
                    o && (E(o, "active", d.autoExpandScrollbar), o = null), c = !1, l && (document.onselectstart = null), t(!0)
                })
            }, M = function() {
                function n(e) {
                    if (!ee(e) || c || S(e)[2]) return void(t = 0);
                    t = 1, b = 0, j = 0, k.removeClass("mCS_touch_action");
                    var n = T.offset();
                    u = S(e)[0] - n.top, d = S(e)[1] - n.left, B = [S(e)[0], S(e)[1]]
                }

                function o(e) {
                    if (ee(e) && !c && !S(e)[2] && (e.stopImmediatePropagation(), !j || b)) {
                        g = q();
                        var t = D.offset(),
                            n = S(e)[0] - t.top,
                            i = S(e)[1] - t.left,
                            o = "mcsLinearOut";
                        if (L.push(n), M.push(i), B[2] = Math.abs(S(e)[0] - B[0]), B[3] = Math.abs(S(e)[1] - B[1]), E.overflowed[0]) var r = _[0].parent().height() - _[0].height(),
                        a = u - n > 0 && n - u > -(r * E.scrollRatio.y) && (2 * B[3] < B[2] || "yx" === x.axis);
                        if (E.overflowed[1]) var s = _[1].parent().width() - _[1].width(),
                        p = d - i > 0 && i - d > -(s * E.scrollRatio.x) && (2 * B[2] < B[3] || "yx" === x.axis);
                        a || p ? (e.preventDefault(), b = 1) : (j = 1, k.addClass("mCS_touch_action")), A = "yx" === x.axis ? [u - n, d - i] : "x" === x.axis ? [null, d - i] : [u - n, null], T[0].idleTimer = 250, E.overflowed[0] && l(A[0], I, o, "y", "all", !0), E.overflowed[1] && l(A[1], I, o, "x", R, !0)
                    }
                }

                function r(e) {
                    if (!ee(e) || c || S(e)[2]) return void(t = 0);
                    t = 1, e.stopImmediatePropagation(), V(k), f = q();
                    var n = D.offset();
                    p = S(e)[0] - n.top, h = S(e)[1] - n.left, L = [], M = []
                }

                function a(e) {
                    if (ee(e) && !c && !S(e)[2]) {
                        e.stopImmediatePropagation(), b = 0, j = 0, m = q();
                        var t = D.offset(),
                            n = S(e)[0] - t.top,
                            i = S(e)[1] - t.left;
                        if (!(m - g > 30)) {
                            v = 1e3 / (m - f);
                            var o = "mcsEaseOut",
                                r = 2.5 > v,
                                a = r ? [L[L.length - 2], M[M.length - 2]] : [0, 0];
                            w = r ? [n - a[0], i - a[1]] : [n - p, i - h];
                            var u = [Math.abs(w[0]), Math.abs(w[1])];
                            v = r ? [Math.abs(w[0] / 4), Math.abs(w[1] / 4)] : [v, v];
                            var d = [Math.abs(T[0].offsetTop) - w[0] * s(u[0] / v[0], v[0]), Math.abs(T[0].offsetLeft) - w[1] * s(u[1] / v[1], v[1])];
                            A = "yx" === x.axis ? [d[0], d[1]] : "x" === x.axis ? [null, d[1]] : [d[0], null], y = [4 * u[0] + x.scrollInertia, 4 * u[1] + x.scrollInertia];
                            var k = parseInt(x.contentTouchScroll) || 0;
                            A[0] = u[0] > k ? A[0] : 0, A[1] = u[1] > k ? A[1] : 0, E.overflowed[0] && l(A[0], y[0], o, "y", R, !1), E.overflowed[1] && l(A[1], y[1], o, "x", R, !1)
                        }
                    }
                }

                function s(e, t) {
                    var n = [1.5 * t, 2 * t, t / 1.5, t / 2];
                    return e > 90 ? t > 4 ? n[0] : n[3] : e > 60 ? t > 3 ? n[3] : n[2] : e > 30 ? t > 8 ? n[1] : t > 6 ? n[0] : t > 4 ? t : n[2] : t > 8 ? t : n[3]
                }

                function l(e, t, n, i, o, r) {
                    e && G(k, e.toString(), {
                        dur: t,
                        scrollEasing: n,
                        dir: i,
                        overwrite: o,
                        drag: r
                    })
                }
                var u, d, p, h, f, g, m, w, v, A, y, b, j, k = e(this),
                    E = k.data(i),
                    x = E.opt,
                    C = i + "_" + E.idx,
                    D = e("#mCSB_" + E.idx),
                    T = e("#mCSB_" + E.idx + "_container"),
                    _ = [e("#mCSB_" + E.idx + "_dragger_vertical"), e("#mCSB_" + E.idx + "_dragger_horizontal")],
                    L = [],
                    M = [],
                    I = 0,
                    R = "yx" === x.axis ? "none" : "all",
                    B = [],
                    N = T.find("iframe"),
                    F = ["touchstart." + C + " pointerdown." + C + " MSPointerDown." + C, "touchmove." + C + " pointermove." + C + " MSPointerMove." + C, "touchend." + C + " pointerup." + C + " MSPointerUp." + C];
                T.bind(F[0], function(e) {
                    n(e)
                }).bind(F[1], function(e) {
                    o(e)
                }), D.bind(F[0], function(e) {
                    r(e)
                }).bind(F[2], function(e) {
                    a(e)
                }), N.length && N.each(function() {
                    e(this).load(function() {
                        P(this) && e(this.contentDocument || this.contentWindow.document).bind(F[0], function(e) {
                            n(e), r(e)
                        }).bind(F[1], function(e) {
                            o(e)
                        }).bind(F[2], function(e) {
                            a(e)
                        })
                    })
                })
            }, I = function() {
                function n() {
                    return window.getSelection ? window.getSelection().toString() : document.selection && "Control" != document.selection.type ? document.selection.createRange().text : 0
                }

                function o(e, t, n) {
                    u.type = n && r ? "stepped" : "stepless", u.scrollAmount = 10, Q(a, e, t, "mcsLinearOut", n ? 60 : null)
                }
                var r, a = e(this),
                    s = a.data(i),
                    l = s.opt,
                    u = s.sequential,
                    d = i + "_" + s.idx,
                    p = e("#mCSB_" + s.idx + "_container"),
                    h = p.parent();
                p.bind("mousedown." + d, function(e) {
                    t || r || (r = 1, c = !0)
                }).add(document).bind("mousemove." + d, function(e) {
                    if (!t && r && n()) {
                        var i = p.offset(),
                            a = S(e)[0] - i.top + p[0].offsetTop,
                            c = S(e)[1] - i.left + p[0].offsetLeft;
                        a > 0 && a < h.height() && c > 0 && c < h.width() ? u.step && o("off", null, "stepped") : ("x" !== l.axis && s.overflowed[0] && (0 > a ? o("on", 38) : a > h.height() && o("on", 40)),
                            "y" !== l.axis && s.overflowed[1] && (0 > c ? o("on", 37) : c > h.width() && o("on", 39)))
                    }
                }).bind("mouseup." + d, function(e) {
                    t || (r && (r = 0, o("off", null)), c = !1)
                })
            }, R = function() {
                function t(t, i) {
                    if (V(n), !B(n, t.target)) {
                        var a = "auto" !== r.mouseWheel.deltaFactor ? parseInt(r.mouseWheel.deltaFactor) : l && t.deltaFactor < 100 ? 100 : t.deltaFactor || 100;
                        if ("x" === r.axis || "x" === r.mouseWheel.axis) var u = "x",
                        d = [Math.round(a * o.scrollRatio.x), parseInt(r.mouseWheel.scrollAmount)], p = "auto" !== r.mouseWheel.scrollAmount ? d[1] : d[0] >= s.width() ? .9 * s.width() : d[0], h = Math.abs(e("#mCSB_" + o.idx + "_container")[0].offsetLeft), f = c[1][0].offsetLeft, g = c[1].parent().width() - c[1].width(), m = t.deltaX || t.deltaY || i;
                        else var u = "y",
                        d = [Math.round(a * o.scrollRatio.y), parseInt(r.mouseWheel.scrollAmount)], p = "auto" !== r.mouseWheel.scrollAmount ? d[1] : d[0] >= s.height() ? .9 * s.height() : d[0], h = Math.abs(e("#mCSB_" + o.idx + "_container")[0].offsetTop), f = c[0][0].offsetTop, g = c[0].parent().height() - c[0].height(), m = t.deltaY || i;
                        "y" === u && !o.overflowed[0] || "x" === u && !o.overflowed[1] || ((r.mouseWheel.invert || t.webkitDirectionInvertedFromDevice) && (m = -m), r.mouseWheel.normalizeDelta && (m = 0 > m ? -1 : 1), (m > 0 && 0 !== f || 0 > m && f !== g || r.mouseWheel.preventDefault) && (t.stopImmediatePropagation(), t.preventDefault()), G(n, (h - m * p).toString(), {
                            dir: u
                        }))
                    }
                }
                if (e(this).data(i)) {
                    var n = e(this),
                        o = n.data(i),
                        r = o.opt,
                        a = i + "_" + o.idx,
                        s = e("#mCSB_" + o.idx),
                        c = [e("#mCSB_" + o.idx + "_dragger_vertical"), e("#mCSB_" + o.idx + "_dragger_horizontal")],
                        u = e("#mCSB_" + o.idx + "_container").find("iframe");
                    u.length && u.each(function() {
                        e(this).load(function() {
                            P(this) && e(this.contentDocument || this.contentWindow.document).bind("mousewheel." + a, function(e, n) {
                                t(e, n)
                            })
                        })
                    }), s.bind("mousewheel." + a, function(e, n) {
                        t(e, n)
                    })
                }
            }, P = function(e) {
                var t = null;
                try {
                    var n = e.contentDocument || e.contentWindow.document;
                    t = n.body.innerHTML
                } catch (i) {}
                return null !== t
            }, B = function(t, n) {
                var o = n.nodeName.toLowerCase(),
                    r = t.data(i).opt.mouseWheel.disableOver,
                    a = ["select", "textarea"];
                return e.inArray(o, r) > -1 && !(e.inArray(o, a) > -1 && !e(n).is(":focus"))
            }, N = function() {
                var t = e(this),
                    n = t.data(i),
                    o = i + "_" + n.idx,
                    r = e("#mCSB_" + n.idx + "_container"),
                    a = r.parent(),
                    s = e(".mCSB_" + n.idx + "_scrollbar ." + u[12]);
                s.bind("touchstart." + o + " pointerdown." + o + " MSPointerDown." + o, function(e) {
                    c = !0
                }).bind("touchend." + o + " pointerup." + o + " MSPointerUp." + o, function(e) {
                    c = !1
                }).bind("click." + o, function(i) {
                    if (e(i.target).hasClass(u[12]) || e(i.target).hasClass("mCSB_draggerRail")) {
                        V(t);
                        var o = e(this),
                            s = o.find(".mCSB_dragger");
                        if (o.parent(".mCSB_scrollTools_horizontal").length > 0) {
                            if (!n.overflowed[1]) return;
                            var l = "x",
                                c = i.pageX > s.offset().left ? -1 : 1,
                                d = Math.abs(r[0].offsetLeft) - .9 * c * a.width()
                        } else {
                            if (!n.overflowed[0]) return;
                            var l = "y",
                                c = i.pageY > s.offset().top ? -1 : 1,
                                d = Math.abs(r[0].offsetTop) - .9 * c * a.height()
                        }
                        G(t, d.toString(), {
                            dir: l,
                            scrollEasing: "mcsEaseInOut"
                        })
                    }
                })
            }, F = function() {
                var t = e(this),
                    n = t.data(i),
                    o = n.opt,
                    r = i + "_" + n.idx,
                    a = e("#mCSB_" + n.idx + "_container"),
                    s = a.parent();
                a.bind("focusin." + r, function(n) {
                    var i = e(document.activeElement),
                        r = a.find(".mCustomScrollBox").length,
                        l = 0;
                    i.is(o.advanced.autoScrollOnFocus) && (V(t), clearTimeout(t[0]._focusTimeout), t[0]._focusTimer = r ? (l + 17) * r : 0, t[0]._focusTimeout = setTimeout(function() {
                        var e = [ne(i)[0], ne(i)[1]],
                            n = [a[0].offsetTop, a[0].offsetLeft],
                            r = [n[0] + e[0] >= 0 && n[0] + e[0] < s.height() - i.outerHeight(!1), n[1] + e[1] >= 0 && n[0] + e[1] < s.width() - i.outerWidth(!1)],
                            c = "yx" !== o.axis || r[0] || r[1] ? "all" : "none";
                        "x" === o.axis || r[0] || G(t, e[0].toString(), {
                            dir: "y",
                            scrollEasing: "mcsEaseInOut",
                            overwrite: c,
                            dur: l
                        }), "y" === o.axis || r[1] || G(t, e[1].toString(), {
                            dir: "x",
                            scrollEasing: "mcsEaseInOut",
                            overwrite: c,
                            dur: l
                        })
                    }, t[0]._focusTimer))
                })
            }, O = function() {
                var t = e(this),
                    n = t.data(i),
                    o = i + "_" + n.idx,
                    r = e("#mCSB_" + n.idx + "_container").parent();
                r.bind("scroll." + o, function(t) {
                    (0 !== r.scrollTop() || 0 !== r.scrollLeft()) && e(".mCSB_" + n.idx + "_scrollbar").css("visibility", "hidden")
                })
            }, U = function() {
                var t = e(this),
                    n = t.data(i),
                    o = n.opt,
                    r = n.sequential,
                    a = i + "_" + n.idx,
                    s = ".mCSB_" + n.idx + "_scrollbar",
                    l = e(s + ">a");
                l.bind("mousedown." + a + " touchstart." + a + " pointerdown." + a + " MSPointerDown." + a + " mouseup." + a + " touchend." + a + " pointerup." + a + " MSPointerUp." + a + " mouseout." + a + " pointerout." + a + " MSPointerOut." + a + " click." + a, function(i) {
                    function a(e, n) {
                        r.scrollAmount = o.snapAmount || o.scrollButtons.scrollAmount, Q(t, e, n)
                    }
                    if (i.preventDefault(), Z(i)) {
                        var s = e(this).attr("class");
                        switch (r.type = o.scrollButtons.scrollType, i.type) {
                            case "mousedown":
                            case "touchstart":
                            case "pointerdown":
                            case "MSPointerDown":
                                if ("stepped" === r.type) return;
                                c = !0, n.tweenRunning = !1, a("on", s);
                                break;
                            case "mouseup":
                            case "touchend":
                            case "pointerup":
                            case "MSPointerUp":
                            case "mouseout":
                            case "pointerout":
                            case "MSPointerOut":
                                if ("stepped" === r.type) return;
                                c = !1, r.dir && a("off", s);
                                break;
                            case "click":
                                if ("stepped" !== r.type || n.tweenRunning) return;
                                a("on", s)
                        }
                    }
                })
            }, Y = function() {
                function t(t) {
                    function i(e, t) {
                        a.type = r.keyboard.scrollType, a.scrollAmount = r.snapAmount || r.keyboard.scrollAmount, "stepped" === a.type && o.tweenRunning || Q(n, e, t)
                    }
                    switch (t.type) {
                        case "blur":
                            o.tweenRunning && a.dir && i("off", null);
                            break;
                        case "keydown":
                        case "keyup":
                            var s = t.keyCode ? t.keyCode : t.which,
                                l = "on";
                            if ("x" !== r.axis && (38 === s || 40 === s) || "y" !== r.axis && (37 === s || 39 === s)) {
                                if ((38 === s || 40 === s) && !o.overflowed[0] || (37 === s || 39 === s) && !o.overflowed[1]) return;
                                "keyup" === t.type && (l = "off"), e(document.activeElement).is(d) || (t.preventDefault(), t.stopImmediatePropagation(), i(l, s))
                            } else if (33 === s || 34 === s) {
                                if ((o.overflowed[0] || o.overflowed[1]) && (t.preventDefault(), t.stopImmediatePropagation()), "keyup" === t.type) {
                                    V(n);
                                    var p = 34 === s ? -1 : 1;
                                    if ("x" === r.axis || "yx" === r.axis && o.overflowed[1] && !o.overflowed[0]) var h = "x",
                                    f = Math.abs(c[0].offsetLeft) - .9 * p * u.width();
                                    else var h = "y",
                                    f = Math.abs(c[0].offsetTop) - .9 * p * u.height();
                                    G(n, f.toString(), {
                                        dir: h,
                                        scrollEasing: "mcsEaseInOut"
                                    })
                                }
                            } else if ((35 === s || 36 === s) && !e(document.activeElement).is(d) && ((o.overflowed[0] || o.overflowed[1]) && (t.preventDefault(), t.stopImmediatePropagation()), "keyup" === t.type)) {
                                if ("x" === r.axis || "yx" === r.axis && o.overflowed[1] && !o.overflowed[0]) var h = "x",
                                f = 35 === s ? Math.abs(u.width() - c.outerWidth(!1)) : 0;
                                else var h = "y",
                                f = 35 === s ? Math.abs(u.height() - c.outerHeight(!1)) : 0;
                                G(n, f.toString(), {
                                    dir: h,
                                    scrollEasing: "mcsEaseInOut"
                                })
                            }
                    }
                }
                var n = e(this),
                    o = n.data(i),
                    r = o.opt,
                    a = o.sequential,
                    s = i + "_" + o.idx,
                    l = e("#mCSB_" + o.idx),
                    c = e("#mCSB_" + o.idx + "_container"),
                    u = c.parent(),
                    d = "input,textarea,select,datalist,keygen,[contenteditable='true']",
                    p = c.find("iframe"),
                    h = ["blur." + s + " keydown." + s + " keyup." + s];
                p.length && p.each(function() {
                    e(this).load(function() {
                        P(this) && e(this.contentDocument || this.contentWindow.document).bind(h[0], function(e) {
                            t(e)
                        })
                    })
                }), l.attr("tabindex", "0").bind(h[0], function(e) {
                    t(e)
                })
            }, Q = function(t, n, o, r, a) {
                function s(e) {
                    var n = "stepped" !== p.type,
                        i = a ? a : e ? n ? g / 1.5 : m : 1e3 / 60,
                        o = e ? n ? 7.5 : 40 : 2.5,
                        l = [Math.abs(h[0].offsetTop), Math.abs(h[0].offsetLeft)],
                        u = [c.scrollRatio.y > 10 ? 10 : c.scrollRatio.y, c.scrollRatio.x > 10 ? 10 : c.scrollRatio.x],
                        d = "x" === p.dir[0] ? l[1] + p.dir[1] * u[1] * o : l[0] + p.dir[1] * u[0] * o,
                        f = "x" === p.dir[0] ? l[1] + p.dir[1] * parseInt(p.scrollAmount) : l[0] + p.dir[1] * parseInt(p.scrollAmount),
                        w = "auto" !== p.scrollAmount ? f : d,
                        v = r ? r : e ? n ? "mcsLinearOut" : "mcsEaseInOut" : "mcsLinear",
                        A = e ? !0 : !1;
                    return e && 17 > i && (w = "x" === p.dir[0] ? l[1] : l[0]), G(t, w.toString(), {
                        dir: p.dir[0],
                        scrollEasing: v,
                        dur: i,
                        onComplete: A
                    }), e ? void(p.dir = !1) : (clearTimeout(p.step), void(p.step = setTimeout(function() {
                        s()
                    }, i)))
                }

                function l() {
                    clearTimeout(p.step), K(p, "step"), V(t)
                }
                var c = t.data(i),
                    d = c.opt,
                    p = c.sequential,
                    h = e("#mCSB_" + c.idx + "_container"),
                    f = "stepped" === p.type ? !0 : !1,
                    g = d.scrollInertia < 26 ? 26 : d.scrollInertia,
                    m = d.scrollInertia < 1 ? 17 : d.scrollInertia;
                switch (n) {
                    case "on":
                        if (p.dir = [o === u[16] || o === u[15] || 39 === o || 37 === o ? "x" : "y", o === u[13] || o === u[15] || 38 === o || 37 === o ? -1 : 1], V(t), te(o) && "stepped" === p.type) return;
                        s(f);
                        break;
                    case "off":
                        l(), (f || c.tweenRunning && p.dir) && s(!0)
                }
            }, H = function(t) {
                var n = e(this).data(i).opt,
                    o = [];
                return "function" == typeof t && (t = t()), t instanceof Array ? o = t.length > 1 ? [t[0], t[1]] : "x" === n.axis ? [null, t[0]] : [t[0], null] : (o[0] = t.y ? t.y : t.x || "x" === n.axis ? null : t, o[1] = t.x ? t.x : t.y || "y" === n.axis ? null : t), "function" == typeof o[0] && (o[0] = o[0]()), "function" == typeof o[1] && (o[1] = o[1]()), o
            }, W = function(t, n) {
                if (null != t && "undefined" != typeof t) {
                    var o = e(this),
                        r = o.data(i),
                        a = r.opt,
                        s = e("#mCSB_" + r.idx + "_container"),
                        l = s.parent(),
                        c = typeof t;
                    n || (n = "x" === a.axis ? "x" : "y");
                    var u = "x" === n ? s.outerWidth(!1) : s.outerHeight(!1),
                        p = "x" === n ? s[0].offsetLeft : s[0].offsetTop,
                        h = "x" === n ? "left" : "top";
                    switch (c) {
                        case "function":
                            return t();
                        case "object":
                            var f = t.jquery ? t : e(t);
                            if (!f.length) return;
                            return "x" === n ? ne(f)[1] : ne(f)[0];
                        case "string":
                        case "number":
                            if (te(t)) return Math.abs(t);
                            if (-1 !== t.indexOf("%")) return Math.abs(u * parseInt(t) / 100);
                            if (-1 !== t.indexOf("-=")) return Math.abs(p - parseInt(t.split("-=")[1]));
                            if (-1 !== t.indexOf("+=")) {
                                var g = p + parseInt(t.split("+=")[1]);
                                return g >= 0 ? 0 : Math.abs(g)
                            }
                            if (-1 !== t.indexOf("px") && te(t.split("px")[0])) return Math.abs(t.split("px")[0]);
                            if ("top" === t || "left" === t) return 0;
                            if ("bottom" === t) return Math.abs(l.height() - s.outerHeight(!1));
                            if ("right" === t) return Math.abs(l.width() - s.outerWidth(!1));
                            if ("first" === t || "last" === t) {
                                var f = s.find(":" + t);
                                return "x" === n ? ne(f)[1] : ne(f)[0]
                            }
                            return e(t).length ? "x" === n ? ne(e(t))[1] : ne(e(t))[0] : (s.css(h, t), void d.update.call(null, o[0]))
                    }
                }
            }, z = function(t) {
                function n() {
                    return clearTimeout(h[0].autoUpdate), 0 === l.parents("html").length ? void(l = null) : void(h[0].autoUpdate = setTimeout(function() {
                        return p.advanced.updateOnSelectorChange && (f = a(), f !== y) ? (s(3), void(y = f)) : (p.advanced.updateOnContentResize && (g = [h.outerHeight(!1), h.outerWidth(!1), w.height(), w.width(), A()[0], A()[1]], (g[0] !== b[0] || g[1] !== b[1] || g[2] !== b[2] || g[3] !== b[3] || g[4] !== b[4] || g[5] !== b[5]) && (s(g[0] !== b[0] || g[1] !== b[1]), b = g)), p.advanced.updateOnImageLoad && (m = o(), m !== j && (h.find("img").each(function() {
                            r(this)
                        }), j = m)), void((p.advanced.updateOnSelectorChange || p.advanced.updateOnContentResize || p.advanced.updateOnImageLoad) && n()))
                    }, p.advanced.autoUpdateTimeout))
                }

                function o() {
                    var e = 0;
                    return p.advanced.updateOnImageLoad && (e = h.find("img").length), e
                }

                function r(t) {
                    function n(e, t) {
                        return function() {
                            return t.apply(e, arguments)
                        }
                    }

                    function i() {
                        this.onload = null, e(t).addClass(u[2]), s(2)
                    }
                    if (e(t).hasClass(u[2])) return void s();
                    var o = new Image;
                    o.onload = n(o, i), o.src = t.src
                }

                function a() {
                    p.advanced.updateOnSelectorChange === !0 && (p.advanced.updateOnSelectorChange = "*");
                    var t = 0,
                        n = h.find(p.advanced.updateOnSelectorChange);
                    return p.advanced.updateOnSelectorChange && n.length > 0 && n.each(function() {
                        t += e(this).height() + e(this).width()
                    }), t
                }

                function s(e) {
                    clearTimeout(h[0].autoUpdate), d.update.call(null, l[0], e)
                }
                var l = e(this),
                    c = l.data(i),
                    p = c.opt,
                    h = e("#mCSB_" + c.idx + "_container");
                if (t) return clearTimeout(h[0].autoUpdate), void K(h[0], "autoUpdate");
                var f, g, m, w = h.parent(),
                    v = [e("#mCSB_" + c.idx + "_scrollbar_vertical"), e("#mCSB_" + c.idx + "_scrollbar_horizontal")],
                    A = function() {
                        return [v[0].is(":visible") ? v[0].outerHeight(!0) : 0, v[1].is(":visible") ? v[1].outerWidth(!0) : 0]
                    }, y = a(),
                    b = [h.outerHeight(!1), h.outerWidth(!1), w.height(), w.width(), A()[0], A()[1]],
                    j = o();
                n()
            }, J = function(e, t, n) {
                return Math.round(e / t) * t - n
            }, V = function(t) {
                var n = t.data(i),
                    o = e("#mCSB_" + n.idx + "_container,#mCSB_" + n.idx + "_container_wrapper,#mCSB_" + n.idx + "_dragger_vertical,#mCSB_" + n.idx + "_dragger_horizontal");
                o.each(function() {
                    X.call(this)
                })
            }, G = function(t, n, o) {
                function r(e) {
                    return l && c.callbacks[e] && "function" == typeof c.callbacks[e]
                }

                function a() {
                    return [c.callbacks.alwaysTriggerOffsets || A >= y[0] + j, c.callbacks.alwaysTriggerOffsets || -k >= A]
                }

                function s() {
                    var e = [h[0].offsetTop, h[0].offsetLeft],
                        n = [w[0].offsetTop, w[0].offsetLeft],
                        i = [h.outerHeight(!1), h.outerWidth(!1)],
                        r = [p.height(), p.width()];
                    t[0].mcs = {
                        content: h,
                        top: e[0],
                        left: e[1],
                        draggerTop: n[0],
                        draggerLeft: n[1],
                        topPct: Math.round(100 * Math.abs(e[0]) / (Math.abs(i[0]) - r[0])),
                        leftPct: Math.round(100 * Math.abs(e[1]) / (Math.abs(i[1]) - r[1])),
                        direction: o.dir
                    }
                }
                var l = t.data(i),
                    c = l.opt,
                    u = {
                        trigger: "internal",
                        dir: "y",
                        scrollEasing: "mcsEaseOut",
                        drag: !1,
                        dur: c.scrollInertia,
                        overwrite: "all",
                        callbacks: !0,
                        onStart: !0,
                        onUpdate: !0,
                        onComplete: !0
                    }, o = e.extend(u, o),
                    d = [o.dur, o.drag ? 0 : o.dur],
                    p = e("#mCSB_" + l.idx),
                    h = e("#mCSB_" + l.idx + "_container"),
                    f = h.parent(),
                    g = c.callbacks.onTotalScrollOffset ? H.call(t, c.callbacks.onTotalScrollOffset) : [0, 0],
                    m = c.callbacks.onTotalScrollBackOffset ? H.call(t, c.callbacks.onTotalScrollBackOffset) : [0, 0];
                if (l.trigger = o.trigger, (0 !== f.scrollTop() || 0 !== f.scrollLeft()) && (e(".mCSB_" + l.idx + "_scrollbar").css("visibility", "visible"), f.scrollTop(0).scrollLeft(0)), "_resetY" !== n || l.contentReset.y || (r("onOverflowYNone") && c.callbacks.onOverflowYNone.call(t[0]), l.contentReset.y = 1), "_resetX" !== n || l.contentReset.x || (r("onOverflowXNone") && c.callbacks.onOverflowXNone.call(t[0]), l.contentReset.x = 1), "_resetY" !== n && "_resetX" !== n) {
                    switch (!l.contentReset.y && t[0].mcs || !l.overflowed[0] || (r("onOverflowY") && c.callbacks.onOverflowY.call(t[0]), l.contentReset.x = null), !l.contentReset.x && t[0].mcs || !l.overflowed[1] || (r("onOverflowX") && c.callbacks.onOverflowX.call(t[0]), l.contentReset.x = null), c.snapAmount && (n = J(n, c.snapAmount, c.snapOffset)), o.dir) {
                        case "x":
                            var w = e("#mCSB_" + l.idx + "_dragger_horizontal"),
                                v = "left",
                                A = h[0].offsetLeft,
                                y = [p.width() - h.outerWidth(!1), w.parent().width() - w.width()],
                                b = [n, 0 === n ? 0 : n / l.scrollRatio.x],
                                j = g[1],
                                k = m[1],
                                x = j > 0 ? j / l.scrollRatio.x : 0,
                                C = k > 0 ? k / l.scrollRatio.x : 0;
                            break;
                        case "y":
                            var w = e("#mCSB_" + l.idx + "_dragger_vertical"),
                                v = "top",
                                A = h[0].offsetTop,
                                y = [p.height() - h.outerHeight(!1), w.parent().height() - w.height()],
                                b = [n, 0 === n ? 0 : n / l.scrollRatio.y],
                                j = g[0],
                                k = m[0],
                                x = j > 0 ? j / l.scrollRatio.y : 0,
                                C = k > 0 ? k / l.scrollRatio.y : 0
                    }
                    b[1] < 0 || 0 === b[0] && 0 === b[1] ? b = [0, 0] : b[1] >= y[1] ? b = [y[0], y[1]] : b[0] = -b[0], t[0].mcs || (s(), r("onInit") && c.callbacks.onInit.call(t[0])), clearTimeout(h[0].onCompleteTimeout), (l.tweenRunning || !(0 === A && b[0] >= 0 || A === y[0] && b[0] <= y[0])) && ($(w[0], v, Math.round(b[1]), d[1], o.scrollEasing), $(h[0], v, Math.round(b[0]), d[0], o.scrollEasing, o.overwrite, {
                        onStart: function() {
                            o.callbacks && o.onStart && !l.tweenRunning && (r("onScrollStart") && (s(), c.callbacks.onScrollStart.call(t[0])), l.tweenRunning = !0, E(w), l.cbOffsets = a())
                        },
                        onUpdate: function() {
                            o.callbacks && o.onUpdate && r("whileScrolling") && (s(), c.callbacks.whileScrolling.call(t[0]))
                        },
                        onComplete: function() {
                            if (o.callbacks && o.onComplete) {
                                "yx" === c.axis && clearTimeout(h[0].onCompleteTimeout);
                                var e = h[0].idleTimer || 0;
                                h[0].onCompleteTimeout = setTimeout(function() {
                                    r("onScroll") && (s(), c.callbacks.onScroll.call(t[0])), r("onTotalScroll") && b[1] >= y[1] - x && l.cbOffsets[0] && (s(), c.callbacks.onTotalScroll.call(t[0])), r("onTotalScrollBack") && b[1] <= C && l.cbOffsets[1] && (s(), c.callbacks.onTotalScrollBack.call(t[0])), l.tweenRunning = !1, h[0].idleTimer = 0, E(w, "hide")
                                }, e)
                            }
                        }
                    }))
                }
            }, $ = function(e, t, n, i, o, r, a) {
                function s() {
                    b.stop || (v || f.call(), v = q() - w, l(), v >= b.time && (b.time = v > b.time ? v + p - (v - b.time) : v + p - 1, b.time < v + 1 && (b.time = v + 1)), b.time < i ? b.id = h(s) : m.call())
                }

                function l() {
                    i > 0 ? (b.currVal = d(b.time, A, j, i, o), y[t] = Math.round(b.currVal) + "px") : y[t] = n + "px", g.call()
                }

                function c() {
                    p = 1e3 / 60, b.time = v + p, h = window.requestAnimationFrame ? window.requestAnimationFrame : function(e) {
                        return l(), setTimeout(e, .01)
                    }, b.id = h(s)
                }

                function u() {
                    null != b.id && (window.requestAnimationFrame ? window.cancelAnimationFrame(b.id) : clearTimeout(b.id), b.id = null)
                }

                function d(e, t, n, i, o) {
                    switch (o) {
                        case "linear":
                        case "mcsLinear":
                            return n * e / i + t;
                        case "mcsLinearOut":
                            return e /= i, e--, n * Math.sqrt(1 - e * e) + t;
                        case "easeInOutSmooth":
                            return e /= i / 2, 1 > e ? n / 2 * e * e + t : (e--, -n / 2 * (e * (e - 2) - 1) + t);
                        case "easeInOutStrong":
                            return e /= i / 2, 1 > e ? n / 2 * Math.pow(2, 10 * (e - 1)) + t : (e--, n / 2 * (-Math.pow(2, -10 * e) + 2) + t);
                        case "easeInOut":
                        case "mcsEaseInOut":
                            return e /= i / 2, 1 > e ? n / 2 * e * e * e + t : (e -= 2, n / 2 * (e * e * e + 2) + t);
                        case "easeOutSmooth":
                            return e /= i, e--, -n * (e * e * e * e - 1) + t;
                        case "easeOutStrong":
                            return n * (-Math.pow(2, -10 * e / i) + 1) + t;
                        case "easeOut":
                        case "mcsEaseOut":
                        default:
                            var r = (e /= i) * e,
                                a = r * e;
                            return t + n * (.499999999999997 * a * r + -2.5 * r * r + 5.5 * a + -6.5 * r + 4 * e)
                    }
                }
                e._mTween || (e._mTween = {
                    top: {},
                    left: {}
                });
                var p, h, a = a || {}, f = a.onStart || function() {}, g = a.onUpdate || function() {}, m = a.onComplete || function() {}, w = q(),
                    v = 0,
                    A = e.offsetTop,
                    y = e.style,
                    b = e._mTween[t];
                "left" === t && (A = e.offsetLeft);
                var j = n - A;
                b.stop = 0, "none" !== r && u(), c()
            }, q = function() {
                return window.performance && window.performance.now ? window.performance.now() : window.performance && window.performance.webkitNow ? window.performance.webkitNow() : Date.now ? Date.now() : (new Date).getTime()
            }, X = function() {
                var e = this;
                e._mTween || (e._mTween = {
                    top: {},
                    left: {}
                });
                for (var t = ["top", "left"], n = 0; n < t.length; n++) {
                    var i = t[n];
                    e._mTween[i].id && (window.requestAnimationFrame ? window.cancelAnimationFrame(e._mTween[i].id) : clearTimeout(e._mTween[i].id), e._mTween[i].id = null, e._mTween[i].stop = 1)
                }
            }, K = function(e, t) {
                try {
                    delete e[t]
                } catch (n) {
                    e[t] = null
                }
            }, Z = function(e) {
                return !(e.which && 1 !== e.which)
            }, ee = function(e) {
                var t = e.originalEvent.pointerType;
                return !(t && "touch" !== t && 2 !== t)
            }, te = function(e) {
                return !isNaN(parseFloat(e)) && isFinite(e)
            }, ne = function(e) {
                var t = e.parents(".mCSB_container");
                return [e.offset().top - t.offset().top, e.offset().left - t.offset().left]
            };
        e.fn[n] = function(t) {
            return d[t] ? d[t].apply(this, Array.prototype.slice.call(arguments, 1)) : "object" != typeof t && t ? void e.error("Method " + t + " does not exist") : d.init.apply(this, arguments)
        }, e[n] = function(t) {
            return d[t] ? d[t].apply(this, Array.prototype.slice.call(arguments, 1)) : "object" != typeof t && t ? void e.error("Method " + t + " does not exist") : d.init.apply(this, arguments)
        }, e[n].defaults = r, window[n] = !0, e(window).load(function() {
            e(o)[n](), e.extend(e.expr[":"], {
                mcsInView: e.expr[":"].mcsInView || function(t) {
                    var n, i, o = e(t),
                        r = o.parents(".mCSB_container");
                    return r.length ? (n = r.parent(), i = [r[0].offsetTop, r[0].offsetLeft], i[0] + ne(o)[0] >= 0 && i[0] + ne(o)[0] < n.height() - o.outerHeight(!1) && i[1] + ne(o)[1] >= 0 && i[1] + ne(o)[1] < n.width() - o.outerWidth(!1)) : void 0
                },
                mcsOverflow: e.expr[":"].mcsOverflow || function(t) {
                    var n = e(t).data(i);
                    return n ? n.overflowed[0] || n.overflowed[1] : void 0
                }
            })
        })
    })
}), $(document).ready(function() {
    var e = $("#bxslider-zoom");
    if (e.bxSlider({
        preventDefaultSwipeY: !1,
        onSliderLoad: function() {},
        nextText: "",
        prevText: "",
        pager: !1,
        controls: !1,
        easing: "ease-in-out",
        speed: 1500
    }), location.search) {
        var t = parseInt(location.search.substring(1).split("&")[0].split("=")[1]);
        e.goToSlide(t)
    } else var t = 0;
    var n = $("#bxslider");
    n.bxSlider({
        preventDefaultSwipeY: !1,
        onSliderLoad: function() {
            $(".juguetes .bxslider-holder .bx-wrapper .bxslider li img").click(function(t) {
                t.preventDefault();
                var n = $(this).attr("data-id");
                $("#register-product-btn").attr({
                    href: $(this).attr("data-href")
                }), $("#like-btn").attr("data-product-id", $(this).attr("data-product-id")), e.goToSlide(n)
            })
        },
        nextText: "",
        prevText: "",
        pager: !1,
        controls: !0,
        easing: "ease-in-out",
        speed: 1e3,
        minSlides: 2,
        maxSlides: 5,
        slideWidth: 135,
        slideMargin: 28
    }), $(".videos .video-thumbs .img-holder .img").click(function(e) {
        e.preventDefault();
        var t = $(this).attr("data-yt"),
            n = $(this).attr("data-video-id");
        initialize_player(n, t)
    }), $(".backstage.karaoke .video-thumbs .img-holder").click(function(e) {
        e.preventDefault(), $(".backstage.karaoke .video-thumbs .img-holder").removeClass("active"), $(this).addClass("active");
        var t = $(this).attr("data-yt");
        jwplayer("jwplayer").setup({
            file: "http://www.youtube.com/watch?v=" + t,
            width: "100%",
            height: "100%",
            skin: {
                name: "five"
            },
            stretching: "exactfit",
            "controlbar.idlehide": "false",
            autostart: !0,
            "youtube.quality": "highres"
        })
    });
    var i = 1,
        o = $(".backstage.canciones .video-thumbs .img-holder").length;
    $(".backstage.canciones .video-thumbs .img-holder").click(function(e) {
        e.preventDefault();
        var t = $(this).attr("data-yt"),
            n = $(this).attr("data-texto");
        i = $(this).attr("data-id"), $(".backstage.canciones .video-thumbs .img-holder").removeClass("active"), $(this).addClass("active"), $(".backstage.canciones .videos-content .videos-holder .player .titulo span").html(n), n = $(this).next().attr("data-texto"), $(".backstage.canciones .videos-content .videos-holder .player .a-continuacion span").html(n), playVideo(t, !0)
    }), $("#prev-video").click(function(e) {
        i--, 1 > i && (i = o);
        var t = $(".backstage.canciones .video-thumbs .img-holder:nth-child(" + i + ")").attr("data-yt");
        $(".backstage.canciones .video-thumbs .img-holder").removeClass("active"), $(".backstage.canciones .video-thumbs .img-holder:nth-child(" + i + ")").addClass("active"), playVideo(t, !0)
    }), $("#next-video").click(function(e) {
        i++, i > o && (i = 1);
        var t = $(".backstage.canciones .video-thumbs .img-holder:nth-child(" + i + ")").attr("data-yt");
        $(".backstage.canciones .video-thumbs .img-holder").removeClass("active"), $(".backstage.canciones .video-thumbs .img-holder:nth-child(" + i + ")").addClass("active"), playVideo(t, !0)
    }), $("#done-btn, .btn-cerrar").click(function() {
        $(".pop").removeClass("show").addClass("hide")
    }), $("#like-btn").click(function() {
        like_product($(this).attr("data-product-id"))
    })
}), $(window).load(function() {
    resizeFix()
}), $(window).scroll(function() {
    onScrollInit()
}), $(window).resize(function() {
    resizeFix()
}),
function() {
    $(".register-toy-btn").click(function() {
        var e = $(this).data("id");
        $("#pop-estas").length > 0 && 1 == ASK_STORY ? ($("#pop-estas").removeClass("hide").addClass("show"), $("#yes-btn").click(function() {
            register_product(e)
        }), $("#no-btn").click(function() {
            $("#pop-estas").removeClass("show").addClass("hide")
        })) : register_product(e)
    }), $(".rollover-efect").length > 0 && $(".rollover-efect").on("mouseover", function() {
        $(this).attr({
            src: $(this).data("rollover")
        })
    }).on("mouseout", function() {
        $(this).attr({
            src: $(this).data("original")
        })
    })
}();
