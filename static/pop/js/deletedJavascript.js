u = function(e) {
    return setTimeout(e, 500)
}, T = function() {
    return document.addEventListener("DOMContentLoaded", function() {
        return Z(i.CHANGE), Z(i.UPDATE)
    }, !0)
}, S = function() {
    return "undefined" != typeof jQuery ? jQuery(document).on("ajaxSuccess", function(e, t, n) {
        return jQuery.trim(t.responseText) ? Z(i.UPDATE) : void 0
    }) : void 0
}, _ = function(e) {
    var t, i;
    return (null != (i = e.state) ? i.turbolinks : void 0) ? (t = I[new n(e.state.url).absolute]) ? (d(), E(t)) : ee(e.target.location.href) : void 0
}, D = function() {
    return z(), W(), document.addEventListener("click", t.installHandlerLast, !0), window.addEventListener("hashchange", function(e) {
        return z(), W()
    }, !1), u(function() {
        return window.addEventListener("popstate", _, !1)
    })
}, C = void 0 !== window.history.state || navigator.userAgent.match(/Firefox\/2[6|7]/), l = window.history && window.history.pushState && window.history.replaceState && C, a = !navigator.userAgent.match(/CriOS\//), G = "GET" === (U = B("request_method")) || "" === U, c = l && a && G, s = document.addEventListener && document.createEvent, s && (T(), S()), c ? (ee = k, D()) : ee = function(e) {
    return document.location.href = e
}, this.Turbolinks = {
    visit: ee,
    pagesCached: P,
    enableTransitionCache: y,
    enableProgressBar: A,
    allowLinkExtensions: o.allowExtensions,
    supported: c,
    EVENTS: f(i)
}
}.call(this);