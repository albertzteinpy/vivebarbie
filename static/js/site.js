function remove_err(){
            $(this).siblings('.err:first').remove();   
}

function liappender(data,box){
    $(box).html('');
    $.each(data,function(ke,valuedict){
            $.each(valuedict,function(namefield,valuefield){
                $(box).append('<li> '+valuefield+'</li>');    
            });
               
        });
   
}

    function closeLbox(e){
        e.preventDefault();
        $('#back_lbox').hide();
        $('#lbox').hide();
        $('#lbox_content').html('last');

    }

    function submiting(e){
        e.preventDefault();
        $(this).parents('form:first').submit();
    }



function reloadpice(changeme){
    var pk = $(changeme).attr('id').replace('pk_','');

    var box = $(changeme).find('.boxlist');
    $.getJSON('/listado/?who='+pk,function(data){
        liappender(data,box);
    });

    
}


function reload_devices()
{
    $('#formularius_domicus_metallicus').before('.');
    if($('.listado').length>0)
        $('.listado').load('/deviceslist');
    else
        location.reload();

}


function login_ok(response){
    if(response.login=='fail'){
        $('.sender').append('<span class="err">Usuario/Contraseña Incorrecta</span>');
    }
    else{
        //window.location.href = "/home";
        //console.log(response.country);
        //location.reload();
        hoster = location.hostname.replace('www.','');
        if(hoster.indexOf('devel')>-1)
            hosta = 'devel.vivebarbie.com';
        else
            hosta = 'vivebarbie.com';

        h = 'http://'+response.country+'.'+ hosta + window.location.pathname;
        window.location.href = h;

    }

}


function saved_alone()
{
    $('#sending').html('<p>Gracias por registrarte.</p><p>Te hemos enviado un correo electrónico, por favor, sigue las instrucciones para activar tu cuenta. :)</p>');

}


function activated_form()
{
    $('#sending').html('<p>Tu cuenta a quedado activada, muchas gracias :).</p>');

}



function sendit(e)
{
    e.preventDefault();
     // EXTRACTING DE FORM DATA
     $(this).before('<div id="sending">Sending...</div>').hide();
     var idform = $(this).attr('id');
     var data = $(this).serializeArray();
     var url = $(this).attr('action');
     var method_is = $(this).attr('method');
     $.ajax({url:url,
         type:method_is,
         data:data,
         dataType:'json',
         success:function(response){
             $('.err').remove();
                if(response.callback){
                    if(typeof(window[response.callback])=='function'){
                        window[response.callback](response);
                    }
                }
                if(response.saved)
                    {
                        document.getElementById(idform).reset();
                    }
                if(response.err) {
                        $('#sending').remove();
                        $('#'+idform).show();
                        $.each(response.data,function(x,y){
                            $('#id_'+x).after('<div class="err"><i class="fa fa-exclamation-triangle">&nbsp;&nbsp;<span>'+y+'</span></i></div>').change(remove_err);
                    });
                }
               
             $('.err:first').siblings().focus();
        }
        });
}
