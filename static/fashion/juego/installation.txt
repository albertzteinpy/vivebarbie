ChalkBoard Painter Pro HTML5 App - 2013 Written By Nolan R Campbell http://www.nuvuthemes.com - support@nuvuthemes.com

This HTML5 Canvas Painting app uses canvas,query-ui,uploadify,fancybox,jquery-mobile to create a browser based painting program with image upload capabilities. Features include - 
Square Brush Tool
Gradient Roll Brush
Colorpicker Push Brush
Invert Effect
Greyscale Effect
Line Tool Brush
Fill Canvas Tool
Brush and Opacity sliders / quick brush select options
Color Spectral Colorpicker
Background tray with image upload - fill canvas with pre-set image or upload your own.
Undo Brush Stroke - undo up to 20 previous brush strokes
App resize with dynamic canvas resizing(optional)
Brush/Color info
Brush Tools Dialog/Modal tooltips
Lightbox render image with image save to png file.
Mobile Browser Support - Safari(apple ipad - minus line tool brush)
Cross Browser support - Safari,Firefox, IE9, Opera, Chrome(image upload bug).
Pattern Picker
Glow Effect


----Installing and Customizing The App-----

Setting your directory to upload image files:
Uploading image files requires your server has php 5.0+ running.
Open js/uploadify/uploadify.php file
Edit line 9 to your uploads folder destination as relative to root.
example: http://google.com would be $targetFolder = '/uploads';
example: http://nrcstudios.info/chalkboard would be $targetFolder = '/chalkboard/uploads';

Make sure your uploads folder permissions are set to "755".

----Scripts and HTML---
All scripts and html on the index.htm page is needed for the app to work.


----Customizing----

Setting App Size:
Change the width and height style properties in the chalkboard div container:
 <!-- BEGIN CHALKBOARD HTML CODE -->
<div class="chalkboard chalkboardpro" modals="true" resize="true" style="width:900px; height:550px; ">


Optional Settings:
<div class="chalkboard" modals="true" resize="true">
You can change the modals and resize tags in the .chalkboard element to turn on/off dialog modals when mouseover brush tools buttons and allow/disallow chalkboard resizing. Set to "false" to turn off, "true" to turn on.

Customizing the ColorpIcker:
You can overwrite the colorpicker.png image in the images folder with your custome png image and the app will use your image for the colorpicker. Its that easy.

Adding Background Images To The Tray:
In the HTML structure find the <!-- EDIT BELOW - ADD SAMPLE IMAGES --> lines 124 of index.htm.
Add images between these lines of code to add preset images to the background images tray.
<!-- EDIT BELOW - ADD SAMPLE IMAGES -->
<!-- EDIT BELOW - ADD SAMPLE IMAGES -->
<!-- EDIT BELOW - ADD SAMPLE IMAGES -->
<img class="backg selectedimg" src="images/sampleimg/logo.jpg"  />
        <img class="backg" src="images/sampleimg/canvas.jpg"  />
        <img class="backg" src="images/sampleimg/7bad0975b3f13dc879201fb573153688.jpg"  />
        <img class="backg" src="images/sampleimg/vortas-.jpg"  />
        <img class="backg" src="images/sampleimg/099c13db0eaba2f5cdb32bc1bb0f6249.jpg"  />
        <img class="backg" src="images/sampleimg/cb1.gif"  />
        <img class="backg" src="images/sampleimg/img9.jpg"  />
        <img class="backg" src="images/sampleimg/74cf815f53c06b1988f69b65b8fc328c.jpg"  />
<!-- EDIT ABOVE - ADD SAMPLE IMAGES -->
<!-- EDIT ABOVE - ADD SAMPLE IMAGES -->
<!-- EDIT ABOVE - ADD SAMPLE IMAGES -->

Adding patterns:
Lines 145+ have the list of pattern images that look like this:
 <img class="selectedpattern" src="images/patterns/leaves.png" style="width:30px; height:30px;" />
    <img  src="images/patterns/spirl1.png" style="width:30px; height:30px;" />
    <img  src="images/patterns/spirl3.png" style="width:30px; height:30px;" />
    <img  src="images/patterns/spirl2.png" style="width:30px; height:30px;" />
    <img  src="images/patterns/flower1.png" style="width:30px; height:30px;" />
    <img  src="images/patterns/flower2.png" style="width:30px; height:30px;" />
    <img  src="images/patterns/star1.png" style="width:30px; height:30px;" />
    <img  src="images/patterns/badge1.png" style="width:30px; height:30px;" />

 You can add or edit these patterns.


----Credits----
Canvas Undo Code - Hristo Yankov - http://hyankov.wordpress.com/2010/12/26/how-to-implement-html5-canvas-undo-function/
Upload Image File - www.uploadify.com
jQuery-UI (dialog) - www.jqueryui.com
Basic Canvas Draw Techniques - http://dev.opera.com/articles/view/html-5-canvas-the-basics/
FancyBox Lightbox - www.fancybox.net
