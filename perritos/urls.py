from django.conf.urls.static import static
from django.conf import settings
from django.conf.urls import *

urlpatterns = patterns('perritos.views',
    url(r'^aventura-perritos/juguetes', 'juguetes', name='juguetes'),
    url(r'^aventura-perritos/perritos_juguete', 'juguete', name='juguete'),
    url(r'^aventura-perritos/descargas', 'descargas', name='descargas'),
    url(r'^aventura-perritos/juego', 'juego', name='juego'),
    url(r'^aventura-perritos/videos', 'videos', name='videos'),
    url(r'^aventura-perritos/download', 'download', name='download'),
     url(r'^aventura-perritos/likes', 'likes', name='likes'),
    url(r'aventura-perritos/', 'home', name='home'),

)

