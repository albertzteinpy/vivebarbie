from django.conf import settings
from django.conf.urls import *

urlpatterns = patterns('mainapp.views',
    # Examples:
    url(r'^$', 'home', name='home'),
    url(r'^sitemap.xml', 'sitemap', name='sitemap'),
    url(r'^pais', 'paises', name='paises'),
    
    url(r'^logueate', 'logueate', name='logueate'),
    url(r'^login', 'login_view', name='login_view'),
    url(r'^registrate', 'registrate', name='registrate'),
    url(r'^activa', 'activa', name='activa'),
    url(r'^conmispapas', 'parentform', name='parentform'),
    url(r'^sola', 'aloneform', name='aloneform'),
    url(r'^form/parent', 'parentform', name='parentform'),
    url(r'^reg/alone', 'reg_alone', name='reg_alone'),
    url(r'^reg/activate', 'parentactv', name='parentactv'),
    url(r'^reg/parent', 'parent_reg', name='parent_reg'),
    url(r'^reg/renueva_reg', 'renueva_reg', name='renueva_reg'),
    url(r'^renueva', 'renueva', name='renueva'),
    url(r'^salir/', 'salir', name='salir'),
    url(r'^perfil/', 'perfil', name='perfil'),
    url(r'^cities', 'cities', name='cities'),
    url(r'^mailing', 'mailing', name='mailing'),
    
    #url(r'^form/', 'form', name='form'),
    #url(r'^setstatus/(?P<iddv>\d+)/$', 'setstatus', name='setstatus'),
    #url(r'^interfaz_form/(?P<iddv>\d+)/', 'interfaz_form', name='interfaz_form'),
    #url(r'^adding/', 'adding', name='adding'),
    #rl(r'^addinterfaz/', 'addinterfaz', name='addinterfaz'),
    #url(r'^device/(?P<iddv>\d+)/$', 'device', name='device'),
    #url(r'^device_form/(?P<iddv>\d+)/$', 'device_form', name='device_form'),
    #url(r'^deviceslist/', 'deviceslist', name='deviceslist'),
    #url(r'^logoff/', 'logoff', name='logoff'),

)



