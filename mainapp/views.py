# -*- encoding: utf-8 -*-
from django.http import HttpResponse
from django.template.context import RequestContext
from django.shortcuts import  render_to_response,render
from django.contrib.auth.decorators import login_required, permission_required
from django.shortcuts import redirect
import string as STrg
import datetime
from mainapp.models import *
import simplejson
from django.db.models import  get_app,get_models
from django.forms.models import model_to_dict
from django.contrib import auth
from django.core.mail import send_mail
from barbie.forms import * 
from django.contrib.auth.models import User
from django.template.loader import render_to_string
from django.contrib.auth.hashers import make_password
from geoip import geolite2
from django.utils import timezone
import pytz
from django.utils.timezone import activate
import csv
import bz2 
import re

FLAGS = {'ar':'flagLatam.png','mx':'flagMx.png','col':'flagCol.png','pe':'flagPe.png','chi':'flagChi.png','latam':'flagLatam.png'}


def mailing(request):
    context = RequestContext(request)
    keyus = request.POST.get('keyus',None)

    if request.COOKIES.has_key( keyus ):
        #value = request.COOKIES[ 'cookie_name' ]
        return HttpResponse('ya se envio')
    else:
        if keyus:
            buser = BarbieUser.objects.get(pk=keyus)
            try:
                mail_to = buser.sysuser.email
            except:
                mail_to = buser.email


            link = 'http://%s/activa?block=%s'%('vivebarbie.com',buser.pk)
            email_body = render_to_string("mailing/registro_sola.html", {'host':'vivebarbie.com','nickname':buser.nickname,'link': link})
            send_mail('Informacion de padres','','Vive Barbie <registro@vivebarbie.com>',[mail_to], fail_silently=False,html_message=email_body)
            response = HttpResponse( 'enviado' )
            max_age = 1 * 24 * 60 * 60
            response.set_cookie( keyus, 'ya se envio' ,max_age=max_age)
            return response
        else:
            assert False,'algo no esta bien'


# VIEWS'S SITE HERE -------------------------------------------------------------------------

def get_country(request):
    ip_user = request.environ.get('HTTP_X_REAL_IP')
    match = geolite2.lookup(ip_user)
    return match.country.lower()

def country_by_url(request):
    hostname = request.environ.get('HTTP_HOST').replace('www.','')
    hpices = hostname.split('.')
    country = Country.objects.all().values('subfix')
    cty = ['%s'%x.values()[0] for x in country]
    if hpices[0] in cty:
        mycountry=hpices[0]
    else:
        mycountry='latam'
    return mycountry

def get_ip(request):
    ip_user = request.environ.get('HTTP_X_REAL_IP')
    return ip_user

def get_title(request,title,principal=' ',onlyhome=''):
    cbyurl = country_by_url(request)
    try:
        pais_name = Country.objects.get(subfix=cbyurl)
    except:
        pais_name = 'LATAM'

    cadena = '%sBarbie%s %s | %s'%(onlyhome,principal,pais_name,title)

    return cadena

def sitemap(request):
    country = country_by_url(request)
    context = RequestContext(request)
    template = 'sitemaps/%s/sitemap.xml'%(country)
    response = render_to_response(template,{},context)
    response['Content-Type'] = 'text/xml; charset=utf-8'
    return response


def home(request):
    args = {}
    context = RequestContext(request)
    country = Country.objects.all().values('subfix')    
    cty = [(str(x['subfix'].lower()),) for x in country]
    getcountry = get_country(request)
    if getcountry in cty[0]:
        mycountry=getcountry
    else:
        mycountry='latam'

    if mycountry=='latam':
        sublink = '/pais'
    else: 
        sublink = ''
    hostname = request.environ.get('HTTP_HOST').replace('www.','')
    hpices = hostname.split('.')
    newlink = 'http://%s.%s'%(mycountry,hostname)

    args['myip']=request.environ.get('HTTP_HOST')
    
    cbyurl = country_by_url(request)
    try:
        pais_name = Country.objects.get(subfix=cbyurl)
    except:
        pais_name = 'LATAM'
    args['titulo']=get_title(request,'Barbie',principal='.com',onlyhome='Vive')

    if hostname=='vivebarbie.com':
        if len(hpices)<3:
            newlink = 'http://%s.vivebarbie.com%s'%(mycountry,sublink)
            return redirect(newlink)
        else:
            request.mycountryflag = FLAGS[country_by_url(request)]
            return render_to_response("home.html",args,context)


    if len(hpices)<4 and hostname=='devel.vivebarbie.com':
        newlink = 'http://%s.devel.vivebarbie.com%s'%(mycountry,sublink)
        return redirect(newlink)
    else:
        request.mycountryflag = FLAGS[country_by_url(request)]
        return render_to_response("home.html",args,context)



def paises(request):
    #ip_user = request.environ.get('HTTP_X_REAL_IP')
    #match = geolite2.lookup(ip_user)
    #tz = match.timezone
    #activate(tz)
    #assert False,timezone.now()
    mycountry = country_by_url(request)
    request.mycountryflag = FLAGS[mycountry]

    args = {}
    args['titulo']='cambio de pais'
    context = RequestContext(request)
    country = Country.objects.all().values('subfix')
    cty = [(str(x['subfix'].lower()),) for x in country]
    getcountry = get_country(request)
    if getcountry in cty[0]:
        mycountry=getcountry
    else:
        mycountry='latam'
    hostname = request.environ.get('HTTP_HOST').replace('www.','')
    hpices = hostname.split('.')
    newlink = 'http://%s.%s'%(mycountry,hostname)
    args['myip']=request.environ.get('HTTP_HOST')

    args['titulo']=get_title(request,'Elije tu país')
    

    '''
    if hostname=='vivebarbie.com':
        if len(hpices)<3:
            newlink = 'http://%s.vivebarbie.com'%mycountry
            return redirect(newlink)
        else:

            return render_to_response("inicio.html",args,context)


    if len(hpices)<4 and hostname=='devel.vivebarbie.com':
        return redirect(newlink)
    else:
    '''
    return render_to_response("inicio.html",args,context)


def logueate(request):
    args = {}
    context = RequestContext(request)
    return render_to_response("forms/login.html",args,context)

def registrate(request):
    args = {}
    context = RequestContext(request)
    args['titulo']='registrate'
    args['titulo']=get_title(request,'Registrate')
    return render_to_response("registro.html",args,context)

'''

    @ Building a Form for kids in alone_mode

'''

def aloneform(request):

    args = {}
    context = RequestContext(request)

    form = FormCreator()
    fields = ['nickname','password','email','name','apaterno','amaterno']
    widgets = {
            'password':forms.HiddenInput(attrs={}),
            'email':forms.TextInput(attrs={}),
            
    }
    
    userform = form.advanced_form_to_model(BarbieUser,fields=fields,widgets=widgets)
    #passcombo = Passimg.objects.all()
    args['uform'] = userform
    #args['passcombo'] = passcombo
    args['titulo']=get_title(request,'Registrate sola')
    return render_to_response("forms/regalone.html",args,context)

'''
    @Saving the kid info in alone_mode

'''

def reg_alone(request):
    data = request.POST.copy()
    correo = request.POST.get('email',None)
    doll = request.POST.get('doll',None)
    figure = request.POST.get('figure',None)

    response = {}
    excludes = {}
    form = FormCreator()
    forma = form.advanced_form_to_model(BarbieUser,excludes=excludes)
    country = get_country(request)
    ip = get_ip(request)
    usernamer = data['nickname']
    countryreg = Country.objects.filter(subfix=country)
    try: 
        usercheck = User.objects.get(nickname=data['nickname'])
    except:
        usercheck =  None


    utemp = User.objects.get(username='saruman')
    extradata = {'ip_last_sign':ip,'country':countryreg,'sign_ip':ip,'sign_count':1,'confirmation_token':1,
                 'register_ip':ip,'sysuser':utemp.pk,'province':' ','adress':' '
                }

    data.update(extradata)

    forma = forma(data)

    if usercheck:
        forma.errors.update({'nickname':'el usuario ya existe'})

    if not correo:
        forma.errors.update({'email':'Este campo es obligatorio.'})        

    if not doll or not figure:
        forma.errors.update({'password':'Tu contraseña esta incompleta.'})        

    if forma.is_valid():
        saved = forma.save()
        usersys,fail = User.objects.get_or_create(username=usernamer,password=make_password(data['password']),email=data['email'])
        usersys.is_staff=True
        usersys.is_active = True
        usersys.save()
        saved.sysuser = usersys
        saved.last_login_at = saved.current_login_at
        saved.current_login_at = timezone.now()
        saved.email=''
        saved.password = make_password(data['password'])
        saved.save()
        keycompres = '%s'%saved.pk
        #keycompres = bz2.compress(keycompres)
        hostname = request.environ.get('HTTP_HOST').replace('www.','')
        link = 'http://%s/activa?block=%s'%(hostname,keycompres)
        email_body = render_to_string("mailing/registro_sola.html", {'host':hostname,'nickname': data['nickname'],'link': link,'pass':(doll,figure)})
        send_mail('Informacion de padres','','Vive Barbie <registro@vivebarbie.com>',[data['email']], fail_silently=False,html_message=email_body)
        usernew = auth.authenticate(username = usernamer, password = data['password'])
        auth.login(request,usernew)
        response['saved']='ok'
        response['callback']='saved_alone'
        
    else:
        response['err']='ok'
        response['data']=forma.errors
    return HttpResponse(simplejson.dumps(response))

def parentform(request):
    args = {}
    context = RequestContext(request)
    nickname = request.GET.get('block',None)
    if nickname:
        instance = BarbieUser.objects.get(pk=nickname)
        initial = instance.email
    else:
        instance = None
        initial = None
    form = FormCreator()
    excludes = ['sysuser','email',
                'enabled','id_sign_ip','register_ip',
                'ip_last_sign','sign_ip','status',
                'sign_count','fecha_confirmacion','confirmation_token']
    pexcludes = ['name']


    COUNTRYS = Country.objects.filter(active=True)

    widgets = {
            'sonsuser':forms.HiddenInput(attrs={'value':instance}),
            'dob':forms.TextInput(attrs={'type':'date'}),
            'password':forms.HiddenInput(attrs={}),
            #'country':forms.TextInput(attrs={}),
            'country':forms.Select(attrs={'class':'registroSelect'}),
            'province':forms.Select(attrs={'class':'registroSelect'}),
    }

    userform = form.advanced_form_to_model(BarbieUser,excludes=excludes,widgets=widgets)
    #userform.country
    parentform = form.advanced_form_to_model(ParentUser,excludes=pexcludes,widgets=widgets)
    userform.base_fields['country'].queryset = Country.objects.filter(active=True)
    args['uform'] = userform(instance=instance)
    args['pform'] = parentform
    args['dias'] = range(1,32)
    args['meses'] = range(1,12)
    args['nmeses'] = ['Mes','Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic']
    args['anyos'] = range(1980,2015)
    args['titulo']=get_title(request,'Registrate con tus papás')
    args['country']=Country.objects.get(subfix=get_country(request))

    return render_to_response("forms/regparent.html",args,context)



def parent_reg(request):

    data = request.POST.copy()
    accept = request.POST.get('parent_accept',None)
    email_parent = request.POST.get('email_parent',None)
    gender = request.POST.get('gender',None)
    province = request.POST.get('province',None)
    phone = request.POST.get('phone',None)
    doll = request.POST.get('doll',None)
    figure = request.POST.get('figure',None)
    adress = request.POST.get('adress',None)
    newsaccept=request.POST.get('newsletter_accept',None)
    response = {}
    excludes = {'sonsuser','email'}
    form = FormCreator()
    forma = form.advanced_form_to_model(BarbieUser,excludes=excludes)
    country = get_country(request)
    ip = get_ip(request)
    usernamer = data['nickname']
    countryreg = Country.objects.filter(subfix=country)
    forma = form.advanced_form_to_model(BarbieUser,excludes=excludes)
    parentform = form.advanced_form_to_model(ParentUser,excludes=excludes)
    
    try:
        dob_mes = int(data['mes'])
    except:
        dob_mes = 0

    dob = '%s-%s-%s'%(data['anyo'],dob_mes,data['dia'])
    try:
        dob = datetime.datetime.strptime(dob, '%Y-%m-%d')
    except:
        dob =None

    try: 
        usercheck = User.objects.get(nickname=data['nickname'])
        sysus = usercheck
    except:
        usercheck =  None

    #validhpne = re.search(r'^\+?1?\d{0,9}$',phone)
    try:
        float(phone)
        validhpne = True
    except ValueError:
        validhpne = False



    if not usercheck:
        sysus = User.objects.get(username='saruman')

    extradata = {'ip_last_sign':ip,'sign_ip':ip,'sign_count':1,
                 'register_ip':ip,'sysuser':sysus.pk,'email_parent':email_parent,'confirmation_token':1,
                }  
    data.update(extradata)        
    forma = forma(data)
    parentform = parentform(data)
    

    if not newsaccept:
        forma.errors.update({'newsletter_accept':u'Elije una opción por favor'})


    if not accept:
        forma.errors.update({'parent_accept':u'Debe aceptar las políticas de privacidad'})
    
    if usercheck:
        forma.errors.update({'nickname':'El usuario ya existe, intenta otro.'})
    

    if not province:
        forma.errors.update({'province':'Este campo es obligatorio.'})


    if not gender:
        forma.errors.update({'gender':'Este campo es obligatorio.'})


    if not doll or not figure:
        forma.errors.update({'password':'Tu contraseña esta incompleta.'})        

    if not validhpne or len(phone)<5:
        forma.errors.update({'phone':'El número de teléfono no es válido.'})

    if not adress:
        forma.errors.update({'adress':u'Este campo es obligatorio.'})

    elif len(adress)<3:
        forma.errors.update({'adress':u'La dirección no es válida.'})



    if forma.is_valid() and parentform.is_valid():
        saved = forma.save()
        usersys = User(username=usernamer)
        usersys.password=make_password(data['password'])
        usersys.is_staff=True
        usersys.email = email_parent
        usersys.save()
        saved.sysuser = usersys
        saved.password = make_password(data['password'])
        saved.save()
        parenter,fail = ParentUser.objects.get_or_create(email_parent=data['email_parent'],parent_accept=True)
        parenter.phone = phone
        if data['newsletter_accept']=='0':
            news = False
        else:
            news = data['newsletter_accept']

        parenter.newsletter_accept = news        
        parenter.sonsuser.add(saved)
        parenter.save()
        hostname = request.environ.get('HTTP_HOST').replace('www.','')
        link = 'http:/%s/activa?block=%s'%(hostname,saved.pk)
        email_body = render_to_string("mailing/registro_sola.html", {'host':hostname,'nickname': data['nickname'],'link': link,'pass':(doll,figure)})
        send_mail('Informacion de padres','Vive Barbie','Vive Barbie <registro@vivebarbie.com>',[data['email_parent']], fail_silently=False,html_message=email_body)
        usernew = auth.authenticate(username = usernamer, password = data['password'])
        auth.login(request,usernew)
        saved.dob=dob
        saved.last_login_at = saved.current_login_at
        saved.current_login_at = timezone.now()
       
        saved.save()
        response['saved']='ok'
        response['callback']='saved_alone'


    else:
        response['err']='ok'
        data = forma.errors
        data.update(parentform.errors)
        response['data']=data
    
    return HttpResponse(simplejson.dumps(response))



def activa(request):
    args = {}
    context = RequestContext(request)
    nickname = request.GET.get('block',None)
    if nickname:
        instance = BarbieUser.objects.get(pk=nickname)
        initial = instance.sysuser.email
    else:
        instance = None
        initial = None
    form = FormCreator()
    excludes = ['password','sysuser','email',
                'enabled','id_sign_ip','register_ip',
                'ip_last_sign','sign_ip','status',
                'sign_count','fecha_confirmacion','confirmation_token']
    pexcludes = ['name']
    widgets = {
            'sonsuser':forms.HiddenInput(attrs={'value':instance.pk}),
            'adress':forms.TextInput(attrs={'value':''}),
            'nickname':forms.TextInput(attrs={'value':instance.nickname,'readonly':'true'}),
            'password':forms.HiddenInput(attrs={}),
            'email_parent':forms.TextInput(attrs={'value':initial,'readonly':'true'}),
            'province':forms.Select(attrs={'class':'registroSelect'}),
            
    }
    userform = form.advanced_form_to_model(BarbieUser,excludes=excludes,widgets=widgets)
    parentform = form.advanced_form_to_model(ParentUser,excludes=pexcludes,widgets=widgets)
    userform.base_fields['country'].queryset = Country.objects.filter(active=True)
    args['uform'] = userform(instance=instance)
    args['pform'] = parentform
    args['dias'] = range(1,32)
    args['meses'] = range(1,12)
    args['nmeses'] = ['Mes','Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic']
    args['anyos'] = range(1980,2015)
    args['instance']=instance   
    args['titulo']=get_title(request,'Activa tu cuenta')
    return render_to_response("forms/parentactivate.html",args,context)


def renueva(request):
    args = {}
    context = RequestContext(request)
    nickname = request.GET.get('block',None)
    nickname = int(nickname,16)
    if nickname:
        instance = BarbieUser.objects.get(pk=nickname)
        initial = instance.sysuser.email
    else:
        instance = None
        initial = None
    form = FormCreator()
    excludes = ['sysuser','email',
                'enabled','id_sign_ip','register_ip',
                'ip_last_sign','sign_ip','status',
                'sign_count','fecha_confirmacion','confirmation_token']
    pexcludes = ['name']
    widgets = {
            'sonsuser':forms.HiddenInput(attrs={'value':instance.pk}),
            'nickname':forms.TextInput(attrs={'value':instance.nickname,'readonly':'true'}),
            'password':forms.HiddenInput(attrs={}),
            'email':forms.TextInput(attrs={'value':''}),
            'email_parent':forms.TextInput(attrs={'value':initial,'readonly':'true'}),
            'province':forms.Select(attrs={'class':'registroSelect'}),
            
    }
    userform = form.advanced_form_to_model(BarbieUser,excludes=excludes,widgets=widgets)
    parentform = form.advanced_form_to_model(ParentUser,excludes=pexcludes,widgets=widgets)
    userform.base_fields['country'].queryset = Country.objects.filter(active=True)
    args['uform'] = userform(instance=instance)
    args['pform'] = parentform
    args['dias'] = range(1,32)
    args['meses'] = range(1,12)
    args['nmeses'] = ['Mes','Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic']
    args['anyos'] = range(1980,2015)
    args['instance']=instance   
    return render_to_response("forms/parentactivate2.html",args,context)



def renueva_reg(request):
    data = request.POST.copy()
    accept = request.POST.get('parent_accept',None)
    gender = request.POST.get('gender',None)
    email_son = request.POST.get('email',None)
    newsaccept = request.POST.get('email_parent',None)
    doll = request.POST.get('doll',None)
    figure = request.POST.get('figure',None)
    adress = request.POST.get('adress',None)
    phone = request.POST.get('phone',None)

    form = FormCreator()
    response = {}
    excludes = {'sonsuser','email'}
    forma = form.advanced_form_to_model(ParentUser,excludes=excludes)
    try: 
        user_son = BarbieUser.objects.get(pk=data['pk'])
    except:
        return HttpResponse('usuario no valido, intente otro')

    try:
        dob_mes = int(data['mes'])
    except:
        dob_mes = 0

    dob = '%s-%s-%s'%(data['anyo'],dob_mes,data['dia'])
    
    try:
        dob = datetime.datetime.strptime(dob, '%Y-%m-%d')
    except:
        dob =None

    forma = forma(data)    

    if not accept:
        forma.errors.update({'parent_accept':u'Debe aceptar las políticas de privacidad'})

    if not gender:
        forma.errors.update({'gender':'Este campo es obligatorio.'})
    
    if  len(adress)<5:
        forma.errors.update({'adress':'Este campo es obligatorio.'})


    if not doll or not figure:
        forma.errors.update({'password':'Tu contraseña esta incompleta.'})        

    try:
        float(phone)
        validhpne = True
    except ValueError:
        validhpne = False



    if not validhpne or len(phone)<5:
        forma.errors.update({'phone':'El número de teléfono no es válido.'})

    
    if not adress:
        forma.errors.update({'adress':u'Este campo es obligatorio.'})

    elif len(adress)<3:
        forma.errors.update({'adress':u'La dirección no es válida.'})


    if forma.is_valid():
        #forma.save()
        sonpk = user_son.pk
        user_son.name = data['name']
        user_son.apaterno = data['apaterno']
        user_son.amaterno = data['amaterno']
        user_son.enabled = 1
        user_son.password = make_password(data['password'])
        user_son.sysuser.password = make_password(data['password'])
        user_son.sysuser.save()
        user_son.status = 1
        #user_son.email = email_son
        user_son.adress = adress
        user_son.gender = gender
        user_son.fecha_confirmacion = datetime.datetime.now()
        user_son.country = Country.objects.get(pk=data['country'])
        user_son.province = data['province']
        user_son.dob = dob
        user_son.sign_ip = get_ip(request)
        user_son.last_login_at = datetime.datetime.now()    
        user_son.save()
        parenter,fail = ParentUser.objects.get_or_create(email_parent=data['email_parent'],parent_accept=True)
        parenter.sonsuser.add(user_son)
        parenter.newsletter_accept = data['newsletter_accept']
        parenter.save()
        response['callback']='activated_form'
        usernew = auth.authenticate(username = user_son.nickname, password = data['password'])
        #if usernew:
        auth.login(request,usernew)
        response['saved']='ok %s'%data['password']

        #auth.login(request,user_son.sysuser)

    else:
        response['err']='ok'
        response['data']=forma.errors
    
    return HttpResponse(simplejson.dumps(response))


#--------------------------


def parentactv(request):
    data = request.POST.copy()
    accept = request.POST.get('parent_accept',None)
    gender = request.POST.get('gender',None)
    email_son = request.POST.get('email',None)
    newsaccept = request.POST.get('email_parent',None)
    adress = request.POST.get('adress',None)
    province = request.POST.get('province',None)
    phone = request.POST.get('phone',None)
    newsaccept = request.POST.get('newsletter_accept',None)

    form = FormCreator()
    response = {}
    excludes = {'sonsuser'}
    forma = form.advanced_form_to_model(ParentUser,excludes=excludes)
    try: 
        user_son = BarbieUser.objects.get(pk=data['pk'])
    except:
        return HttpResponse('usuario no valido, intente otro')

    try:
        dob_mes = int(data['mes'])
    except:
        dob_mes = 0

    dob = '%s-%s-%s'%(data['anyo'],dob_mes,data['dia'])
    
    try:
        dob = datetime.datetime.strptime(dob, '%Y-%m-%d')
    except:
        dob =None

    forma = forma(data)    
    
    try:
        float(phone)
        validhpne = True
    except ValueError:
        validhpne = False


    if not validhpne or len(phone)<5:
        forma.errors.update({'phone':'El número de teléfono no es válido.'})

    if not adress:
        forma.errors.update({'adress':u'Este campo es obligatorio.'})
    
    elif len(adress)<3:
        forma.errors.update({'adress':u'La dirección no es válida.'})

    if not newsaccept:
        forma.errors.update({'newsletter_accept':u'Elije Una opción por favor'})


    if not accept:
        forma.errors.update({'parent_accept':u'Debe aceptar las políticas de privacidad'})

    if not gender:
        forma.errors.update({'gender':'Este campo es obligatorio.'})
    
    if not province:
        forma.errors.update({'province':'Este campo es obligatorio.'})    



    if forma.is_valid():
        #forma.save()
        sonpk = user_son.pk
        user_son.name = data['name']
        user_son.enabled = 1
        user_son.status = 1
        user_son.email = email_son
        user_son.gender = gender
        user_son.fecha_confirmacion = datetime.datetime.now()
        user_son.country = Country.objects.get(pk=data['country'])
        user_son.province = data['province']
        user_son.dob = dob
        user_son.save()
        parenter,fail = ParentUser.objects.get_or_create(email_parent=data['email_parent'],parent_accept=True)
        parenter.sonsuser.add(user_son)
        if data['newsletter_accept']=='0':
            news = False
        else:
            news = data['newsletter_accept']

        parenter.newsletter_accept = news
        parenter.save()
        response['news'] = parenter.newsletter_accept
        response['saved']='ok'
        response['callback']='activated_form'
        #auth.login(request,user_son.sysuser)

    else:
        response['err']='ok'
        response['data']=forma.errors
    
    return HttpResponse(simplejson.dumps(response))
 
def login_view(request):
    ip = request.environ['REMOTE_ADDR']
    username = request.POST.get('user', '')
    password = request.POST.get('pw', '')
    response = {}
    user = auth.authenticate(username = username, password = password)   
    if user is not None:
        auth.login(request,user)
        buser = user.barbieuser_set.all()[0]
        buser.last_login_at = buser.current_login_at
        buser.current_login_at = timezone.now()
        buser.sign_count += 1
        buser.save()

        response['login']='ok'
        response['country']=buser.country.subfix
        response['callback'] = 'login_ok'
    else:
        response['err']='ok'
        response['data']={'nologin':'Usuario y/o Contrsaeña incorrecta'}
        
    
    return HttpResponse(simplejson.dumps(response))


def perfil(request):
    args = {}
    context = RequestContext(request)
    instance = request.user.barbieuser_set.all()[0]
    

    form = FormCreator()
    excludes = ['sysuser','email','password',
                'enabled','id_sign_ip','register_ip',
                'ip_last_sign','sign_ip','status',
                'sign_count','fecha_confirmacion','confirmation_token']
    pexcludes = ['name']
    widgets = {
            'sonsuser':forms.HiddenInput(),
            'password':forms.PasswordInput(),
    }

    userform = form.advanced_form_to_model(BarbieUser,excludes=excludes,widgets=widgets)
    parentform = form.advanced_form_to_model(ParentUser,excludes=pexcludes,widgets=widgets)
    #passcombo = Passimg.objects.all()
    args['uform'] = userform(instance=instance)
    args['pform'] = parentform
    args['instance']=instance
    #args['passcombo'] = passcombo
    args['titulo']='perfil'
    return render_to_response("perfil.html",args,context)



def salir(request):
    auth.logout(request)
    return redirect('/')




def cities(request):
    pid = request.GET.get('p',None)
    response = {}
    if pid:
        cities_list = City.objects.filter(country_id=pid)
        cs = [{'city':x.city_name} for x in cities_list]
        response['cs']=cs
    else:
        response['fail']='ok'
    return HttpResponse(simplejson.dumps(response))



def importer():
    cr = csv.reader(open("paises.csv","rb"))
    a = []
    for row in cr:
        print row[1]
        c,fail = Country.objects.get_or_create(contry_name=u'%s'%row[1].decode('utf-8'))
        ciudad,fcity = City.objects.get_or_create(city_name=row[4].decode('utf-8'),country_id=c)
        print row[2]
    print 'finish'    
    #return HttpResponse(a)




def import_scores():
    cr = csv.reader(open("scores.csv","rb"))
    response = {}
    usersys = {}
    for row in cr:
        try: 
            u = BarbieUser.objects.get(nickname=row[0])
            print row
            try:
                dob = datetime.datetime.strptime(row[6], '%Y-%m-%d %H:%M:%S')
            except:
                print row[6]
                return False
            
            score_user,fail_score = Scoreuser.objects.get_or_create(usuariobarbie=u,score = row[5],created_at = '%s'%dob)
            if score_user:
                score_user.description='%s'%row[1]
                score_user.save()
        except:
            pass

    print 'finish'    




def import_user():
    cr = csv.reader(open("testuser.csv","rb"))
    response = {}
    usersys = {}
    for row in cr:
        u = BarbieUser.objects.filter(nickname=row[1])
        if not u:
            print 'se puede migrar'
            
            try: 
                countryset = Country.objects.get(subfix=row[19])
            except: 
                countryset = Country.objects.get(subfix='latam')


            try:
                dob = datetime.datetime.strptime(row[5], '%Y-%m-%d')
            except:
                dob = None

            usersys['username']=row[1]
            usersys['password']='not update'
            usersys['email']=row[7]

            userSy = User(**usersys)
            userSy.save()
            response['nickname']=row[1]
            if dob: response['dob'] = dob
            response['sysuser']=userSy
            response['gender'] = row[6]
            response['name'] = row[2]
            response['apaterno'] = row[2]
            response['amaterno'] = row[2]
            response['email'] = row[7]
            response['password'] = 'not updated'
            response['sign_count'] = row[8]
            response['sign_date'] = datetime.datetime.now()
            response['confirmation_token'] = 0
            response['fecha_confirmacion'] = None
            response['country'] = countryset
            response['province'] = ''
            response['status'] = ''
            response['sign_ip'] = ''
            response['register_ip'] = row[12]
            response['ip_last_sign'] = row[10]
            response['current_login_at'] = row[9]
            response['last_login_at'] = row[10]
            response['enabled'] = False


            m = BarbieUser(**response)
            print response
            m.save()
            
            print m


    print 'finish'    


def send_user():
    us = BarbieUser.objects.filter(confirmation_token=False,password='not updated')
    if len(us)>10:
        lep = 10
    else:
        lep = len(us)-1




    for u in us[:lep]:

        hostname = 'devel.vivebarbie.com'
        link = 'http:/%s/renueva?block=%s'%(hostname,hex(u.pk))
        email_body = render_to_string("mailing/campanya_preregistro.html", {'host':hostname,'nickname': u.nickname,'link': link})
        u.confirmation_token=True
        u.save()
        print u
        send_mail('Campaña','Vive Barbie','Vive Barbie <registro@vivebarbie.com>',[u.email], fail_silently=False,html_message=email_body)

    return us
