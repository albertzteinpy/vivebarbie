# -*- encoding: utf-8 -*-
from django.db import models
from django.contrib.auth.models import User


GENRERS = ((u'Niño',u'Niño'),(u'Niña',u'Niña'))


class Country(models.Model):

	contry_name = models.CharField(u'Nombre',max_length=200)
	subfix = models.CharField(u'Subfijo',max_length=5)
	active = models.BooleanField(default=False)
	order = models.IntegerField(u'orden',blank=True,null=True,default=0)
	def __unicode__(self):
		return u'%s'%(self.contry_name)


class City(models.Model):
	country_id = models.ForeignKey(Country)
	city_name = models.CharField(u'Nombre',max_length=200)
	active = models.BooleanField(default=False)
	order = models.IntegerField(u'orden',blank=True,null=True,default=0)
	subfix_city = models.CharField(u'Subfijo',max_length=5,default='0')
	def __unicode__(self):
		return u'%s'%(self.city_name)



class Passimg(models.Model):
	rowup = models.ImageField(u'imagen renglon arriba',upload_to='pass')
	keyup = models.CharField(u'valor',max_length=50)
	rowdown = models.ImageField(u'Imagen abajo',upload_to='pass')
	keydown = models.CharField(u'valor',max_length=50)	


class BarbieUser(models.Model):
	sysuser = models.ForeignKey(User)
	name = models.CharField(u'Nombre de tu hija(o):',max_length=200)
	apaterno = models.CharField(u'Apellido Paterno:',max_length=200,default='')
	amaterno = models.CharField(u'Apellido Materno:',max_length=200,default='')
	adress = models.CharField(u'direccion:',max_length=200,default='')
	nickname = models.CharField(u'nickname',max_length=200,unique=True)
	enabled = models.BooleanField(u'enabled',max_length=200)
	dob	 = models.DateField(u'Fecha de Cumpleaños',max_length=200,blank=True,null=True)
	gender = models.CharField(u'Genero',max_length=10,choices = GENRERS,blank=True,null=True)
	password = models.CharField(u'Contraseña',max_length=200)
	sign_count = models.IntegerField('contadorLogin',default=1)
	sign_date = models.DateTimeField('agregado',auto_now_add=True)
	confirmation_token = models.BooleanField('confirmacion enviada',default=0)
	fecha_confirmacion = models.DateTimeField('confirmado en',blank=True,null=True)
	country = models.ForeignKey(Country)
	province = models.CharField(u'Provincia',max_length=200,default='')
	status = models.BooleanField('estatus',default=0)
	sign_ip = models.CharField('IP_logueo_actual',max_length=50)
	register_ip = models.CharField('IP_registro',max_length=50)
	ip_last_sign = models.CharField('ultima_ip',max_length=50)
	email = models.EmailField(u'Tu correo electrónico',max_length=200,default='',blank=True,null=True)
	current_login_at = models.DateTimeField('current login',blank=True,null=True)
	last_login_at = models.DateTimeField('last login',blank=True,null=True)
	total_score = models.IntegerField('score',default=0,blank=True,null=True)


	def __unicode__(self):
		return '%s %s'%(self.nickname,self.country)


class ParentUser(models.Model):
	sonsuser = models.ManyToManyField(BarbieUser)
	name = models.CharField(u'Nombre',max_length=200,default='')
	parent_accept = models.BooleanField(u'acepta terminos')
	newsletter_accept = models.BooleanField(u'Desea recibir correos',default=False)
	email_parent = models.EmailField(u'Tu correo electrónico',max_length=200)
	phone = models.CharField(u'Telefono',max_length=50,default='')



class Scoreuser(models.Model):
	usuariobarbie = models.ForeignKey(BarbieUser,related_name='scoreusuario',blank=True,null=True)
	description = models.TextField(blank=True,null=True)
	score = models.IntegerField(blank=True,null=True,default=0)
	created_at = models.DateTimeField(auto_now_add=True)
	campain = models.CharField(max_length=200,blank=True,null=True)


	def __unicode__(self):
		return self.email_parent



