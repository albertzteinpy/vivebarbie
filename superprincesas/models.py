# -*- encoding: utf-8 -*-
from django.db import models
from django.contrib.auth.models import User
from mainapp.models import * 


DESCARGABLES_CAT = [('0','Brazaletes'),
					('1','Colorear'),
					('2','Wallpapers'),
					]


class Video(models.Model):

	background =  models.ImageField(u'Imagen movil',upload_to='perritos__videos')
	preview_ogv =  models.FileField(u'Pre ogv',upload_to='perritos__videos')
	preview_webm =  models.FileField(u'Pre webm',upload_to='perritos__videos')
	preview_mp4 =  models.FileField(u'Pre mp4',upload_to='perritos__videos')
	preview = models.ImageField(u'Preview carrusel',upload_to='perritos_preview_video')
	url = models.CharField(u'link Video',max_length=255)
	titulo = models.CharField(u'Título',max_length=255)
	puntos = models.IntegerField(u'Valor de Puntos',default=0)
	status = models.BooleanField(u'Visible',default=True)
	country = models.ManyToManyField(Country,related_name='prcv')

	def __unicode__(self):
		return u'%s'%self.titulo

class Toy(models.Model):
	nombre = models.CharField(u'Nombre',max_length=255)
	codigo = models.CharField(u'Codigo',max_length=255)
	descp = models.TextField(u'Descripción')
	puntos = models.IntegerField(u'Valor de Puntos')
	country = models.ManyToManyField(Country,related_name='prct')
	imagen =  models.ImageField(u'Imagen',upload_to='princesas_toys')
	titulo =  models.CharField(u'Título',max_length=200)
	texto =  models.CharField(u'Texto',max_length=200)
	imagen_car =  models.ImageField(u'Imagen Carousel',upload_to='princesas_toys')
	imagen_roll =  models.ImageField(u'Imagen Roll Over',upload_to='princesas_toys',blank=True,null=True)
	imagen_big =  models.ImageField(u'Imagen Big',upload_to='princesas_toys',blank=True,null=True)
	likes = models.IntegerField(u'likes',default=0,blank=True,null=True)
	def __unicode__(self):
		return u'%s'%self.nombre


class Descargable(models.Model):
	nombre = models.CharField(u'Nombre',max_length=255)
	codigo = models.CharField(u'Codigo',max_length=255)
	descp = models.TextField(u'Descripción')
	puntos_para_desbloquear = models.IntegerField(u'Valor de Puntos')
	country = models.ManyToManyField(Country,related_name='prcd')
	imagen =  models.ImageField(u'Imagen',upload_to='perritos_descargas')
	preview =  models.ImageField(u'Imagen preview',upload_to='perritos_descargas')
	imagen_opc =  models.ImageField(u'Imagen Opcional',upload_to='perritos_descargas')
	cat = models.CharField(u'Categoría',max_length=10,choices=DESCARGABLES_CAT,default='0')

	def __unicode__(self):
		return u'%s'%self.nombre



class Bulikes(models.Model):
	userliker = models.ForeignKey(BarbieUser)
	toylike = models.ForeignKey(Toy)
	date_like = models.DateTimeField(u'cuando',auto_now_add=True)