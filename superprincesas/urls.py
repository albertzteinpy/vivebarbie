from django.conf.urls.static import static
from django.conf import settings
from django.conf.urls import *

urlpatterns = patterns('superprincesas.views',
    url(r'^super-princesas/juguetes', 'juguetes', name='juguetes'),
    url(r'^super-princesas/perritos_juguete', 'juguete', name='juguete'),
    url(r'^super-princesas/descargas', 'descargas', name='descargas'),
    url(r'^super-princesas/juego', 'juego', name='juego'),
    url(r'^super-princesas/videos', 'videos', name='videos'),
    url(r'^super-princesas/download', 'download', name='download'),
    url(r'^super-princesas/likes', 'likes', name='likes'),
    url(r'^super-princesas/', 'home', name='home'),

)

