# -*- encoding: utf-8 -*-
from django.http import HttpResponse
from django.template.context import RequestContext
from django.shortcuts import get_object_or_404, render_to_response,render
from django.contrib.auth.decorators import login_required, permission_required
from django.shortcuts import redirect
import string as STrg
import datetime
from mainapp.models import *
from superprincesas.models import * 
import simplejson
from django.db.models import  get_app,get_models
from django.forms.models import model_to_dict
from django.contrib import auth
from django.core.mail import send_mail
from barbie.forms import * 
from django.contrib.auth.models import User
from mainapp.views import get_country,get_ip,country_by_url,FLAGS,get_title
from django.db.models import Q
from io import BytesIO
from StringIO import StringIO
import mimetypes
from PIL import Image
import os
from slugify import slugify




def download(request):
    filekey = request.GET.get('f',None)
    filename = '%s/static/superprincesas/%s'%(os.path.dirname(os.path.dirname(os.path.abspath(__file__))),filekey)
    mm = mimetypes.guess_extension(filename)
    mtype = mimetypes.guess_type(filename)
    downloadame,extension = os.path.splitext(filename)
    datename = '%s'%datetime.datetime.now()
    nuname = '%s%s'%(slugify(datename),extension)
    f = open(filename)
    response = HttpResponse(f,content_type=mtype[0])

    dname_list = downloadame.split('/')
    dname = dname_list[len(dname_list)-1]

    response['Content-Disposition'] = 'attachment; filename="%s%s"'%(dname,extension)

    
    return response

# VIEWS'S SITE HERE -------------------------------------------------------------------------

def home(request):
    args = {}
    context = RequestContext(request)
    args['titulo']=' Inicio'
    mycountry = country_by_url(request)
    request.mycountryflag = FLAGS[mycountry]
    args['titulo']=get_title(request,'Super Princesas')
    return render_to_response("princesas_home.html",args,context)

def juguetes(request):
    args = {}
    context = RequestContext(request)
    country = country_by_url(request)
    real_country = get_country(request)

    if real_country == country:
        args['liketoy'] = True
    else:
        args['liketoy'] = None 
    if not country:
        country = real_country
    toys = Toy.objects.filter(country__subfix='%s'%country)
    args['toys']=toys
    args['titulo']=' Juguetes'
    try:
        likeds = Bulikes.objects.filter(userliker=request.user.barbieuser_set.all()[0])
        likeds_ids = [x.toylike.pk for x in likeds]
    except:
        likeds_ids = []

    
    args['likeds']=likeds_ids
    mycountry = country_by_url(request)
    request.mycountryflag = FLAGS[mycountry]
    args['titulo']=get_title(request,'Muñecas Super Princesas')
    return render_to_response("princesas_juguetes.html",args,context)


def juguete(request):
    args = {}
    context = RequestContext(request)
    id = request.GET.get('pk',None)
    try:
    	toys = Toy.objects.get(pk=id)
    except:
    	return HttpResponse('No toy regsitered')
    args['toys']=toys
    return render_to_response("princesas_juguete.html",args,context)


def descargas(request):
    args = {}
    context = RequestContext(request)
    '''
    country = country_by_url(request)
    real_country = get_country(request)

    if real_country == country:
        args['liketoy'] = True
    else:
        args['liketoy'] = None 
    if not country:
        country = real_country
    downs = Descargable.objects.filter(country__subfix='%s'%country)
    args['downs'] = {}
    args['downs']['br']=downs.filter(cat='0')
    args['downs']['cr']=downs.filter(cat='1')
    args['downs']['w']=downs.filter(cat='2')
    '''
    args['titulo']=' Descargas'
    mycountry = country_by_url(request)
    request.mycountryflag = FLAGS[mycountry]
    args['titulo']=get_title(request,'Descargas Super Princesas')
    return render_to_response("princesas_descargas.html",args,context)


def juego(request):
	args = {}
	context = RequestContext(request)
    
	return render_to_response("juego.html",args,context)


def videos(request):
    args= {}
    context = RequestContext(request)
    args['titulo']=' Videos'
    mycountry = country_by_url(request)
    request.mycountryflag = FLAGS[mycountry]
    args['titulo']=get_title(request,'Videos Super Princesas')
    return render_to_response("princesas_videos.html",args,context)



def likes(request):
    toyliked = request.GET.get('t',None)

    if toyliked and request.user.is_staff:
        toyliker = Toy.objects.get(pk=toyliked)

        try: 
            barbieuser = request.user.barbieuser_set.all()[0]
        except:
            return HttpResponse('no es usuario valido')
    
        try:
            liked = Bulikes.objects.get(userliker=barbieuser,toylike=toyliker)
            return HttpResponse('ya le diste like')
        except:
            liked = Bulikes(userliker=barbieuser,toylike=toyliker)
            toyliker.likes += 1
            liked.save()
            toyliker.save()
            return HttpResponse('dandole like like')


    else:
        return HttpResponse('no like')

        